ARG IMAGE_NODE=node:20.11-bookworm-slim


FROM $IMAGE_NODE as build

WORKDIR /app

COPY package.json yarn.lock ./
RUN yarn install

COPY nx.json tsconfig.base.json ./
COPY ./apps ./apps
RUN npx nx build manga



FROM $IMAGE_NODE

WORKDIR /app
COPY --from=build /app/dist .
ENTRYPOINT ["node", "apps/manga/server/server.mjs"]
