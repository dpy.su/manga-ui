import { gql } from 'apollo-angular';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  /** Date with time (isoformat) */
  DateTime: { input: string; output: string; }
  UUID: { input: string; output: string; }
  Upload: { input: any; output: any; }
};

export type AltTitle = {
  __typename?: 'AltTitle';
  id: Scalars['ID']['output'];
  language: LanguageEnum;
  title: Scalars['String']['output'];
};

export type ChapterCreateError = EntityAlreadyExistsError | FileUploadError | PermissionDeniedError | RelationshipNotFoundError | ValidationErrors;

export type ChapterCreateInput = {
  branchId: Scalars['ID']['input'];
  number: Array<Scalars['Int']['input']>;
  title: Scalars['String']['input'];
  volume?: InputMaybe<Scalars['Int']['input']>;
};

export type ChapterCreatePayload = {
  __typename?: 'ChapterCreatePayload';
  chapter?: Maybe<MangaChapter>;
  error?: Maybe<ChapterCreateError>;
};

export type ChapterMutationGql = {
  __typename?: 'ChapterMutationGQL';
  create: ChapterCreatePayload;
};


export type ChapterMutationGqlCreateArgs = {
  input: ChapterCreateInput;
  pages: Array<Scalars['Upload']['input']>;
};

export type EntityAlreadyExistsError = Error & {
  __typename?: 'EntityAlreadyExistsError';
  message: Scalars['String']['output'];
};

export type Error = {
  message: Scalars['String']['output'];
};

export type FileUploadError = Error & {
  __typename?: 'FileUploadError';
  message: Scalars['String']['output'];
};

export type Group = {
  __typename?: 'Group';
  id: Scalars['ID']['output'];
  name: Scalars['String']['output'];
};

export type GroupCreateError = EntityAlreadyExistsError | ValidationErrors;

export type GroupCreateInput = {
  name: Scalars['String']['input'];
};

export type GroupCreatePayload = {
  __typename?: 'GroupCreatePayload';
  error?: Maybe<GroupCreateError>;
  group?: Maybe<Group>;
};

export type GroupMutations = {
  __typename?: 'GroupMutations';
  create: GroupCreatePayload;
};


export type GroupMutationsCreateArgs = {
  input: GroupCreateInput;
};

export type Image = {
  __typename?: 'Image';
  height: Scalars['Int']['output'];
  id: Scalars['ID']['output'];
  url: Scalars['String']['output'];
  width: Scalars['Int']['output'];
};

export type ImageSet = {
  __typename?: 'ImageSet';
  alternatives: Array<Image>;
  id: Scalars['ID']['output'];
  original: Image;
};

export enum LanguageEnum {
  Eng = 'ENG',
  Jpn = 'JPN',
  Ukr = 'UKR'
}

export type Manga = {
  __typename?: 'Manga';
  altTitles: Array<AltTitle>;
  arts: Array<MangaArt>;
  bookmark?: Maybe<MangaBookmark>;
  bookmarkCount: Scalars['Int']['output'];
  chapters: MangaChapterPagePaginationResult;
  commentCount: Scalars['Int']['output'];
  coverArt?: Maybe<MangaArt>;
  createdAt: Scalars['DateTime']['output'];
  description: Scalars['String']['output'];
  id: Scalars['ID']['output'];
  language: LanguageEnum;
  myRating?: Maybe<MangaRating>;
  rating: Scalars['Float']['output'];
  ratingCount: Scalars['Int']['output'];
  status: MangaStatus;
  tags: Array<MangaTag>;
  title: Scalars['String']['output'];
  titleSlug: Scalars['String']['output'];
  updatedAt: Scalars['DateTime']['output'];
};


export type MangaChaptersArgs = {
  pagination?: InputMaybe<PagePaginationInput>;
};

export type MangaAddBookmarkInput = {
  mangaId: Scalars['ID']['input'];
  status: MangaBookmarkStatus;
};

export type MangaArt = {
  __typename?: 'MangaArt';
  id: Scalars['ID']['output'];
  image: ImageSet;
  language: LanguageEnum;
  volume: Scalars['Int']['output'];
};

export type MangaArtAddInput = {
  image: Scalars['Upload']['input'];
  language: LanguageEnum;
  volume: Scalars['Int']['input'];
};

export type MangaArtsAddError = EntityAlreadyExistsError | FileUploadError | NotFoundError | PermissionDeniedError | ValidationErrors;

export type MangaArtsAddInput = {
  arts: Array<MangaArtAddInput>;
  mangaId: Scalars['ID']['input'];
};

export type MangaArtsAddPayload = {
  __typename?: 'MangaArtsAddPayload';
  error?: Maybe<MangaArtsAddError>;
  manga?: Maybe<Manga>;
};

export type MangaBookmark = {
  __typename?: 'MangaBookmark';
  createdAt: Scalars['DateTime']['output'];
  id: Scalars['ID']['output'];
  manga: Manga;
  status: MangaBookmarkStatus;
};

export type MangaBookmarkError = NotFoundError | ValidationErrors;

export type MangaBookmarkFilter = {
  manga?: InputMaybe<MangaFilter>;
  statuses?: InputMaybe<Array<MangaBookmarkStatus>>;
};

export type MangaBookmarkPagePaginationResult = {
  __typename?: 'MangaBookmarkPagePaginationResult';
  items: Array<MangaBookmark>;
  pageInfo: PagePaginationInfo;
};

export type MangaBookmarkPayload = {
  __typename?: 'MangaBookmarkPayload';
  error?: Maybe<MangaBookmarkError>;
  manga?: Maybe<Manga>;
};

export type MangaBookmarkSort = {
  direction?: SortDirection;
  field?: MangaBookmarkSortField;
};

export enum MangaBookmarkSortField {
  BookmarkAddedAt = 'BOOKMARK_ADDED_AT',
  ChapterUpload = 'CHAPTER_UPLOAD',
  CreatedAt = 'CREATED_AT',
  Title = 'TITLE'
}

export enum MangaBookmarkStatus {
  Completed = 'COMPLETED',
  Dropped = 'DROPPED',
  PlanToRead = 'PLAN_TO_READ',
  Reading = 'READING'
}

export type MangaBranch = {
  __typename?: 'MangaBranch';
  id: Scalars['ID']['output'];
  language: LanguageEnum;
  name: Scalars['String']['output'];
};

export type MangaBranchCreateError = RelationshipNotFoundError | ValidationErrors;

export type MangaBranchCreateInput = {
  groupId: Scalars['ID']['input'];
  language: LanguageEnum;
  mangaId: Scalars['ID']['input'];
  name: Scalars['String']['input'];
};

export type MangaBranchCreatePayload = {
  __typename?: 'MangaBranchCreatePayload';
  branch?: Maybe<MangaBranch>;
  error?: Maybe<MangaBranchCreateError>;
};

export type MangaBranchMutationGql = {
  __typename?: 'MangaBranchMutationGQL';
  create: MangaBranchCreatePayload;
};


export type MangaBranchMutationGqlCreateArgs = {
  input: MangaBranchCreateInput;
};

export type MangaChapter = {
  __typename?: 'MangaChapter';
  id: Scalars['ID']['output'];
  number: Scalars['String']['output'];
  pages: Array<MangaPage>;
  title: Scalars['String']['output'];
  volume?: Maybe<Scalars['Int']['output']>;
};

export type MangaChapterPagePaginationResult = {
  __typename?: 'MangaChapterPagePaginationResult';
  items: Array<MangaChapter>;
  pageInfo: PagePaginationInfo;
};

export type MangaCreateError = EntityAlreadyExistsError | PermissionDeniedError | ValidationErrors;

export type MangaCreateInput = {
  description: Scalars['String']['input'];
  language: LanguageEnum;
  status: MangaStatus;
  title: Scalars['String']['input'];
};

export type MangaCreatePayload = {
  __typename?: 'MangaCreatePayload';
  error?: Maybe<MangaCreateError>;
  manga?: Maybe<Manga>;
};

export type MangaFilter = {
  searchTerm?: InputMaybe<Scalars['String']['input']>;
  statuses?: InputMaybe<Array<MangaStatus>>;
  tags?: InputMaybe<MangaTagFilter>;
};

export type MangaMutations = {
  __typename?: 'MangaMutations';
  addArts: MangaArtsAddPayload;
  addBookmark: MangaBookmarkPayload;
  create: MangaCreatePayload;
  removeBookmark: MangaBookmarkPayload;
  setCoverArt: MangaSetCoverArtPayload;
  setRating: MangaSetRatingPayload;
  update: MangaUpdatePayload;
};


export type MangaMutationsAddArtsArgs = {
  input: MangaArtsAddInput;
};


export type MangaMutationsAddBookmarkArgs = {
  input: MangaAddBookmarkInput;
};


export type MangaMutationsCreateArgs = {
  input: MangaCreateInput;
};


export type MangaMutationsRemoveBookmarkArgs = {
  id: Scalars['ID']['input'];
};


export type MangaMutationsSetCoverArtArgs = {
  input: MangaSetCoverArtInput;
};


export type MangaMutationsSetRatingArgs = {
  input: MangaSetRatingInput;
};


export type MangaMutationsUpdateArgs = {
  input: MangaUpdateInput;
};

export type MangaPage = {
  __typename?: 'MangaPage';
  Id: Scalars['UUID']['output'];
  id: Scalars['ID']['output'];
  image: ImageSet;
  number: Scalars['Int']['output'];
};

export type MangaPagePaginationResult = {
  __typename?: 'MangaPagePaginationResult';
  items: Array<Manga>;
  pageInfo: PagePaginationInfo;
};

export type MangaRating = {
  __typename?: 'MangaRating';
  id: Scalars['ID']['output'];
  value: Scalars['Int']['output'];
};

export type MangaSetCoverArtErrorGql = NotFoundError | PermissionDeniedError | ValidationErrors;

export type MangaSetCoverArtInput = {
  artId?: InputMaybe<Scalars['ID']['input']>;
  mangaId: Scalars['ID']['input'];
};

export type MangaSetCoverArtPayload = {
  __typename?: 'MangaSetCoverArtPayload';
  error?: Maybe<MangaSetCoverArtErrorGql>;
  manga?: Maybe<Manga>;
};

export type MangaSetRatingError = NotFoundError | ValidationErrors;

export type MangaSetRatingInput = {
  mangaId: Scalars['ID']['input'];
  rating?: InputMaybe<Scalars['Int']['input']>;
};

export type MangaSetRatingPayload = {
  __typename?: 'MangaSetRatingPayload';
  error?: Maybe<MangaSetRatingError>;
  manga?: Maybe<Manga>;
  rating?: Maybe<MangaRating>;
};

export type MangaSort = {
  direction?: SortDirection;
  field?: MangaSortField;
};

export enum MangaSortField {
  ChapterUpload = 'CHAPTER_UPLOAD',
  CreatedAt = 'CREATED_AT',
  Title = 'TITLE'
}

export enum MangaStatus {
  Cancelled = 'CANCELLED',
  Completed = 'COMPLETED',
  Ongoing = 'ONGOING',
  OnHold = 'ON_HOLD'
}

export type MangaTag = {
  __typename?: 'MangaTag';
  category: MangaTagCategory;
  id: Scalars['ID']['output'];
  name: Scalars['String']['output'];
  slug: Scalars['String']['output'];
};

export type MangaTagCategory = {
  __typename?: 'MangaTagCategory';
  id: Scalars['ID']['output'];
  name: Scalars['String']['output'];
};

export type MangaTagFilter = {
  exclude?: InputMaybe<Array<Scalars['ID']['input']>>;
  include?: InputMaybe<Array<Scalars['ID']['input']>>;
};

export type MangaUpdateError = EntityAlreadyExistsError | NotFoundError | PermissionDeniedError | ValidationErrors;

export type MangaUpdateInput = {
  description: Scalars['String']['input'];
  id: Scalars['ID']['input'];
  language: LanguageEnum;
  status: MangaStatus;
  title: Scalars['String']['input'];
};

export type MangaUpdatePayload = {
  __typename?: 'MangaUpdatePayload';
  error?: Maybe<MangaUpdateError>;
  manga?: Maybe<Manga>;
};

export type Mutation = {
  __typename?: 'Mutation';
  branches: MangaBranchMutationGql;
  chapters: ChapterMutationGql;
  groups: GroupMutations;
  manga: MangaMutations;
  user: UserMutations;
};

export type NotFoundError = Error & {
  __typename?: 'NotFoundError';
  entityId: Scalars['ID']['output'];
  message: Scalars['String']['output'];
};

export type PagePaginationInfo = {
  __typename?: 'PagePaginationInfo';
  currentPage: Scalars['Int']['output'];
  hasNextPage: Scalars['Boolean']['output'];
  hasPreviousPage: Scalars['Boolean']['output'];
  pageSize: Scalars['Int']['output'];
  totalItems: Scalars['Int']['output'];
  totalPages: Scalars['Int']['output'];
};

export type PagePaginationInput = {
  page?: Scalars['Int']['input'];
  pageSize?: Scalars['Int']['input'];
};

export type PermissionDeniedError = Error & {
  __typename?: 'PermissionDeniedError';
  message: Scalars['String']['output'];
};

export type PrivateUser = User & {
  __typename?: 'PrivateUser';
  avatar?: Maybe<ImageSet>;
  email: Scalars['String']['output'];
  id: Scalars['ID']['output'];
  joinedAt: Scalars['DateTime']['output'];
  username: Scalars['String']['output'];
};

export type Query = {
  __typename?: 'Query';
  chapter?: Maybe<MangaChapter>;
  manga?: Maybe<Manga>;
  mangas: MangaPagePaginationResult;
  me: PrivateUser;
  myBookmarks: MangaBookmarkPagePaginationResult;
  tags: Array<MangaTag>;
};


export type QueryChapterArgs = {
  id: Scalars['ID']['input'];
};


export type QueryMangaArgs = {
  id: Scalars['ID']['input'];
};


export type QueryMangasArgs = {
  filter?: InputMaybe<MangaFilter>;
  pagination?: InputMaybe<PagePaginationInput>;
  sort?: InputMaybe<MangaSort>;
};


export type QueryMyBookmarksArgs = {
  filter?: InputMaybe<MangaBookmarkFilter>;
  pagination?: InputMaybe<PagePaginationInput>;
  sort?: InputMaybe<MangaBookmarkSort>;
};

export type RelationshipNotFoundError = Error & {
  __typename?: 'RelationshipNotFoundError';
  entityId: Scalars['ID']['output'];
  message: Scalars['String']['output'];
};

export enum SortDirection {
  Asc = 'ASC',
  Desc = 'DESC'
}

export type User = {
  avatar?: Maybe<ImageSet>;
  id: Scalars['ID']['output'];
  joinedAt: Scalars['DateTime']['output'];
  username: Scalars['String']['output'];
};

export type UserAvatarChangeError = FileUploadError;

export type UserAvatarChangePayload = {
  __typename?: 'UserAvatarChangePayload';
  error?: Maybe<UserAvatarChangeError>;
  user?: Maybe<PrivateUser>;
};

export type UserMutations = {
  __typename?: 'UserMutations';
  changeAvatar: UserAvatarChangePayload;
};


export type UserMutationsChangeAvatarArgs = {
  avatar: Scalars['Upload']['input'];
};

export type ValidationError = Error & {
  __typename?: 'ValidationError';
  code: Scalars['String']['output'];
  location: Array<Scalars['String']['output']>;
  message: Scalars['String']['output'];
};

export type ValidationErrors = Error & {
  __typename?: 'ValidationErrors';
  errors: Array<ValidationError>;
  message: Scalars['String']['output'];
};

export type SimpleSearchQueryVariables = Exact<{
  term: Scalars['String']['input'];
}>;


export type SimpleSearchQuery = { __typename?: 'Query', mangas: { __typename?: 'MangaPagePaginationResult', items: Array<{ __typename?: 'Manga', status: MangaStatus, id: string, title: string, titleSlug: string, language: LanguageEnum, bookmarkCount: number, commentCount: number, rating: number, coverArt?: { __typename?: 'MangaArt', image: { __typename?: 'ImageSet', original: { __typename?: 'Image', width: number, height: number, url: string }, alternatives: Array<{ __typename?: 'Image', url: string, width: number }> } } | null }> } };

export type SimpleSearchPreviewFragment = { __typename?: 'Manga', status: MangaStatus, id: string, title: string, titleSlug: string, language: LanguageEnum, bookmarkCount: number, commentCount: number, rating: number, coverArt?: { __typename?: 'MangaArt', image: { __typename?: 'ImageSet', original: { __typename?: 'Image', width: number, height: number, url: string }, alternatives: Array<{ __typename?: 'Image', url: string, width: number }> } } | null };

export type MangaTagsQueryVariables = Exact<{ [key: string]: never; }>;


export type MangaTagsQuery = { __typename?: 'Query', tags: Array<{ __typename?: 'MangaTag', id: string, name: string, slug: string, category: { __typename?: 'MangaTagCategory', id: string, name: string } }> };

export type AdvancedSearchQueryVariables = Exact<{
  filter: MangaFilter;
  sort?: InputMaybe<MangaSort>;
  pagination: PagePaginationInput;
}>;


export type AdvancedSearchQuery = { __typename?: 'Query', mangas: { __typename?: 'MangaPagePaginationResult', items: Array<{ __typename?: 'Manga', description: string, status: MangaStatus, id: string, title: string, titleSlug: string, language: LanguageEnum, bookmarkCount: number, commentCount: number, rating: number, tags: Array<{ __typename?: 'MangaTag', id: string, name: string, slug: string }>, coverArt?: { __typename?: 'MangaArt', image: { __typename?: 'ImageSet', original: { __typename?: 'Image', width: number, height: number, url: string }, alternatives: Array<{ __typename?: 'Image', url: string, width: number }> } } | null }>, pageInfo: { __typename?: 'PagePaginationInfo', pageSize: number, currentPage: number, totalItems: number, hasNextPage: boolean, hasPreviousPage: boolean, totalPages: number } } };

export type BookmarksQueryVariables = Exact<{
  status: MangaBookmarkStatus;
  pagination: PagePaginationInput;
}>;


export type BookmarksQuery = { __typename?: 'Query', myBookmarks: { __typename?: 'MangaBookmarkPagePaginationResult', items: Array<{ __typename?: 'MangaBookmark', id: string, status: MangaBookmarkStatus, manga: { __typename?: 'Manga', description: string, status: MangaStatus, id: string, title: string, titleSlug: string, language: LanguageEnum, bookmarkCount: number, commentCount: number, rating: number, tags: Array<{ __typename?: 'MangaTag', id: string, name: string, slug: string }>, coverArt?: { __typename?: 'MangaArt', image: { __typename?: 'ImageSet', original: { __typename?: 'Image', width: number, height: number, url: string }, alternatives: Array<{ __typename?: 'Image', url: string, width: number }> } } | null } }>, pageInfo: { __typename?: 'PagePaginationInfo', pageSize: number, currentPage: number, totalItems: number, hasNextPage: boolean, hasPreviousPage: boolean, totalPages: number } } };

export type LatestUpdatesSectionQueryVariables = Exact<{ [key: string]: never; }>;


export type LatestUpdatesSectionQuery = { __typename?: 'Query', mangas: { __typename?: 'MangaPagePaginationResult', items: Array<{ __typename?: 'Manga', description: string, status: MangaStatus, id: string, title: string, titleSlug: string, language: LanguageEnum, bookmarkCount: number, commentCount: number, rating: number, tags: Array<{ __typename?: 'MangaTag', id: string, name: string, slug: string }>, coverArt?: { __typename?: 'MangaArt', image: { __typename?: 'ImageSet', original: { __typename?: 'Image', width: number, height: number, url: string }, alternatives: Array<{ __typename?: 'Image', url: string, width: number }> } } | null }> } };

export type MangaChapterItemFragment = { __typename?: 'MangaChapter', id: string, volume?: number | null, number: string, title: string };

export type AddBookmarkMutationVariables = Exact<{
  input: MangaAddBookmarkInput;
}>;


export type AddBookmarkMutation = { __typename?: 'Mutation', manga: { __typename?: 'MangaMutations', addBookmark: { __typename?: 'MangaBookmarkPayload', error?: { __typename?: 'NotFoundError', message: string } | { __typename?: 'ValidationErrors', message: string } | null } } };

export type RemoveBookmarkMutationVariables = Exact<{
  mangaId: Scalars['ID']['input'];
}>;


export type RemoveBookmarkMutation = { __typename?: 'Mutation', manga: { __typename?: 'MangaMutations', removeBookmark: { __typename?: 'MangaBookmarkPayload', error?: { __typename?: 'NotFoundError', message: string } | { __typename?: 'ValidationErrors', message: string } | null } } };

export type UserBookmarkFragment = { __typename?: 'Manga', id: string, bookmark?: { __typename?: 'MangaBookmark', id: string, status: MangaBookmarkStatus } | null };

export type MangaChaptersQueryVariables = Exact<{
  mangaId: Scalars['ID']['input'];
  pagination: PagePaginationInput;
}>;


export type MangaChaptersQuery = { __typename?: 'Query', manga?: { __typename?: 'Manga', id: string, chapters: { __typename?: 'MangaChapterPagePaginationResult', items: Array<{ __typename?: 'MangaChapter', id: string, volume?: number | null, number: string, title: string }>, pageInfo: { __typename?: 'PagePaginationInfo', pageSize: number, currentPage: number, totalItems: number, hasNextPage: boolean, hasPreviousPage: boolean, totalPages: number } } } | null };

export type MangaDescriptionFragment = { __typename?: 'Manga', description: string };

export type MangaInfoQueryVariables = Exact<{
  mangaId: Scalars['ID']['input'];
}>;


export type MangaInfoQuery = { __typename?: 'Query', manga?: { __typename?: 'Manga', status: MangaStatus, id: string, title: string, titleSlug: string, language: LanguageEnum, description: string, bookmarkCount: number, commentCount: number, rating: number, tags: Array<{ __typename?: 'MangaTag', id: string, name: string, slug: string }>, coverArt?: { __typename?: 'MangaArt', image: { __typename?: 'ImageSet', original: { __typename?: 'Image', width: number, height: number, url: string }, alternatives: Array<{ __typename?: 'Image', url: string, width: number }> } } | null, bookmark?: { __typename?: 'MangaBookmark', id: string, status: MangaBookmarkStatus } | null, myRating?: { __typename?: 'MangaRating', id: string, value: number } | null } | null };

export type MangaInfoFragment = { __typename?: 'Manga', status: MangaStatus, id: string, title: string, titleSlug: string, language: LanguageEnum, description: string, bookmarkCount: number, commentCount: number, rating: number, tags: Array<{ __typename?: 'MangaTag', id: string, name: string, slug: string }>, coverArt?: { __typename?: 'MangaArt', image: { __typename?: 'ImageSet', original: { __typename?: 'Image', width: number, height: number, url: string }, alternatives: Array<{ __typename?: 'Image', url: string, width: number }> } } | null, bookmark?: { __typename?: 'MangaBookmark', id: string, status: MangaBookmarkStatus } | null, myRating?: { __typename?: 'MangaRating', id: string, value: number } | null };

export type SetMangaRatingMutationVariables = Exact<{
  input: MangaSetRatingInput;
}>;


export type SetMangaRatingMutation = { __typename?: 'Mutation', manga: { __typename?: 'MangaMutations', setRating: { __typename?: 'MangaSetRatingPayload', error?: { __typename?: 'NotFoundError', message: string } | { __typename?: 'ValidationErrors', message: string } | null } } };

export type UserRatingFragment = { __typename?: 'Manga', id: string, myRating?: { __typename?: 'MangaRating', id: string, value: number } | null };

export type MangaCoverFragment = { __typename?: 'Manga', id: string, title: string, titleSlug: string, coverArt?: { __typename?: 'MangaArt', image: { __typename?: 'ImageSet', original: { __typename?: 'Image', width: number, height: number, url: string }, alternatives: Array<{ __typename?: 'Image', url: string, width: number }> } } | null };

export type MangaStatsFragment = { __typename?: 'Manga', bookmarkCount: number, commentCount: number, rating: number };

export type MangaTagFragment = { __typename?: 'MangaTag', id: string, name: string, slug: string };

export type MangaTileFragment = { __typename?: 'Manga', description: string, status: MangaStatus, id: string, title: string, titleSlug: string, language: LanguageEnum, bookmarkCount: number, commentCount: number, rating: number, tags: Array<{ __typename?: 'MangaTag', id: string, name: string, slug: string }>, coverArt?: { __typename?: 'MangaArt', image: { __typename?: 'ImageSet', original: { __typename?: 'Image', width: number, height: number, url: string }, alternatives: Array<{ __typename?: 'Image', url: string, width: number }> } } | null };

export type MangaTitleFragment = { __typename?: 'Manga', id: string, title: string, titleSlug: string, language: LanguageEnum };

export const MangaTitleFragmentDoc = gql`
    fragment MangaTitleFragment on Manga {
  id
  title
  titleSlug
  language
}
    `;
export const MangaStatsFragmentDoc = gql`
    fragment MangaStatsFragment on Manga {
  bookmarkCount
  commentCount
  rating
}
    `;
export const MangaCoverFragmentDoc = gql`
    fragment MangaCoverFragment on Manga {
  id
  title
  titleSlug
  coverArt {
    image {
      original {
        width
        height
        url
      }
      alternatives {
        url
        width
      }
    }
  }
}
    `;
export const SimpleSearchPreviewFragmentDoc = gql`
    fragment SimpleSearchPreviewFragment on Manga {
  ...MangaTitleFragment
  status
  ...MangaStatsFragment
  ...MangaCoverFragment
}
    ${MangaTitleFragmentDoc}
${MangaStatsFragmentDoc}
${MangaCoverFragmentDoc}`;
export const MangaChapterItemFragmentDoc = gql`
    fragment MangaChapterItemFragment on MangaChapter {
  id
  volume
  number
  title
}
    `;
export const MangaDescriptionFragmentDoc = gql`
    fragment MangaDescriptionFragment on Manga {
  description
}
    `;
export const UserBookmarkFragmentDoc = gql`
    fragment UserBookmarkFragment on Manga {
  id
  bookmark {
    id
    status
  }
}
    `;
export const UserRatingFragmentDoc = gql`
    fragment UserRatingFragment on Manga {
  id
  myRating {
    id
    value
  }
}
    `;
export const MangaTagFragmentDoc = gql`
    fragment MangaTagFragment on MangaTag {
  id
  name
  slug
}
    `;
export const MangaInfoFragmentDoc = gql`
    fragment MangaInfoFragment on Manga {
  ...MangaTitleFragment
  ...MangaCoverFragment
  ...MangaDescriptionFragment
  ...UserBookmarkFragment
  ...UserRatingFragment
  status
  ...MangaStatsFragment
  tags {
    ...MangaTagFragment
  }
}
    ${MangaTitleFragmentDoc}
${MangaCoverFragmentDoc}
${MangaDescriptionFragmentDoc}
${UserBookmarkFragmentDoc}
${UserRatingFragmentDoc}
${MangaStatsFragmentDoc}
${MangaTagFragmentDoc}`;
export const MangaTileFragmentDoc = gql`
    fragment MangaTileFragment on Manga {
  ...MangaTitleFragment
  description
  status
  ...MangaStatsFragment
  tags {
    ...MangaTagFragment
  }
  ...MangaCoverFragment
}
    ${MangaTitleFragmentDoc}
${MangaStatsFragmentDoc}
${MangaTagFragmentDoc}
${MangaCoverFragmentDoc}`;
export const SimpleSearchDocument = gql`
    query SimpleSearch($term: String!) {
  mangas(filter: {searchTerm: $term}, pagination: {pageSize: 5}) {
    items {
      ...SimpleSearchPreviewFragment
    }
  }
}
    ${SimpleSearchPreviewFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class SimpleSearchGQL extends Apollo.Query<SimpleSearchQuery, SimpleSearchQueryVariables> {
    override document = SimpleSearchDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const MangaTagsDocument = gql`
    query MangaTags {
  tags {
    id
    name
    slug
    category {
      id
      name
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class MangaTagsGQL extends Apollo.Query<MangaTagsQuery, MangaTagsQueryVariables> {
    override document = MangaTagsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const AdvancedSearchDocument = gql`
    query AdvancedSearch($filter: MangaFilter!, $sort: MangaSort, $pagination: PagePaginationInput!) {
  mangas(filter: $filter, sort: $sort, pagination: $pagination) {
    items {
      ...MangaTileFragment
    }
    pageInfo {
      pageSize
      currentPage
      totalItems
      hasNextPage
      hasPreviousPage
      totalPages
    }
  }
}
    ${MangaTileFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class AdvancedSearchGQL extends Apollo.Query<AdvancedSearchQuery, AdvancedSearchQueryVariables> {
    override document = AdvancedSearchDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const BookmarksDocument = gql`
    query Bookmarks($status: MangaBookmarkStatus!, $pagination: PagePaginationInput!) {
  myBookmarks(
    filter: {statuses: [$status]}
    sort: {field: BOOKMARK_ADDED_AT, direction: DESC}
    pagination: $pagination
  ) {
    items {
      id
      status
      manga {
        ...MangaTileFragment
      }
    }
    pageInfo {
      pageSize
      currentPage
      totalItems
      hasNextPage
      hasPreviousPage
      totalPages
    }
  }
}
    ${MangaTileFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class BookmarksGQL extends Apollo.Query<BookmarksQuery, BookmarksQueryVariables> {
    override document = BookmarksDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const LatestUpdatesSectionDocument = gql`
    query LatestUpdatesSection {
  mangas(
    sort: {direction: DESC, field: CHAPTER_UPLOAD}
    pagination: {pageSize: 20}
  ) {
    items {
      ...MangaTileFragment
    }
  }
}
    ${MangaTileFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class LatestUpdatesSectionGQL extends Apollo.Query<LatestUpdatesSectionQuery, LatestUpdatesSectionQueryVariables> {
    override document = LatestUpdatesSectionDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const AddBookmarkDocument = gql`
    mutation addBookmark($input: MangaAddBookmarkInput!) {
  manga {
    addBookmark(input: $input) {
      error {
        ... on Error {
          message
        }
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class AddBookmarkGQL extends Apollo.Mutation<AddBookmarkMutation, AddBookmarkMutationVariables> {
    override document = AddBookmarkDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RemoveBookmarkDocument = gql`
    mutation removeBookmark($mangaId: ID!) {
  manga {
    removeBookmark(id: $mangaId) {
      error {
        ... on Error {
          message
        }
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RemoveBookmarkGQL extends Apollo.Mutation<RemoveBookmarkMutation, RemoveBookmarkMutationVariables> {
    override document = RemoveBookmarkDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const MangaChaptersDocument = gql`
    query MangaChapters($mangaId: ID!, $pagination: PagePaginationInput!) {
  manga(id: $mangaId) {
    id
    chapters(pagination: $pagination) {
      items {
        ...MangaChapterItemFragment
      }
      pageInfo {
        pageSize
        currentPage
        totalItems
        hasNextPage
        hasPreviousPage
        totalPages
      }
    }
  }
}
    ${MangaChapterItemFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class MangaChaptersGQL extends Apollo.Query<MangaChaptersQuery, MangaChaptersQueryVariables> {
    override document = MangaChaptersDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const MangaInfoDocument = gql`
    query MangaInfo($mangaId: ID!) {
  manga(id: $mangaId) {
    ...MangaInfoFragment
  }
}
    ${MangaInfoFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class MangaInfoGQL extends Apollo.Query<MangaInfoQuery, MangaInfoQueryVariables> {
    override document = MangaInfoDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const SetMangaRatingDocument = gql`
    mutation setMangaRating($input: MangaSetRatingInput!) {
  manga {
    setRating(input: $input) {
      error {
        ... on Error {
          message
        }
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class SetMangaRatingGQL extends Apollo.Mutation<SetMangaRatingMutation, SetMangaRatingMutationVariables> {
    override document = SetMangaRatingDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }