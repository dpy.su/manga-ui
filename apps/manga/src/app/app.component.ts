import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {
  MenuComponent,
  SidenavComponent,
  SimpleSearchBarComponent,
  ToolbarComponent,
} from './layout';

@Component({
  selector: 'moe-root',
  standalone: true,
  templateUrl: './app.component.html',
  imports: [
    RouterModule,
    CommonModule,
    SidenavComponent,
    ToolbarComponent,
    MenuComponent,
    SimpleSearchBarComponent,
  ],
  styleUrl: './app.component.scss',
})
export class AppComponent {

}
