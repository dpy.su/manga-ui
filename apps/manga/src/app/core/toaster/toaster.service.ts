import { inject, Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { concatMap, map, Observable, Subject, switchMap, take } from 'rxjs';
import {
  CustomSnackbarComponent,
  CustomSnackbarData,
} from './custom-snackbar/custom-snackbar.component';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { TranslocoService } from '@jsverse/transloco';

type ToastConfig = {
  translationKey: string;
  data: Omit<CustomSnackbarData, 'message'>;
  panelClass?: string | string[] | undefined;
};

@Injectable({
  providedIn: 'root',
})
export class Toaster {
  private toastQueue = new Subject<ToastConfig>();

  private snackBar = inject(MatSnackBar);
  private translateService = inject(TranslocoService);

  constructor() {
    this.toastQueue
      .pipe(
        concatMap((cfg) => this.openToast(cfg)),
        takeUntilDestroyed()
      )
      .subscribe();
  }

  public showInfo(translationKey: string): void {
    const cfg = {
      data: { iconType: 'info', hideAfter: 3000 },
      translationKey,
    } satisfies ToastConfig;

    this.toastQueue.next(cfg);
  }

  public showWarning(translationKey: string): void {
    const cfg = {
      data: { iconType: 'warning', hideAfter: 3000 },
      translationKey,
    } satisfies ToastConfig;

    this.toastQueue.next(cfg);
  }

  public showError(translationKey = 'TOASTER_ERRORS.UNEXPECTED'): void {
    const cfg = {
      data: { iconType: 'error', hideAfter: 3000 },
      translationKey,
    } satisfies ToastConfig;

    this.toastQueue.next(cfg);
  }

  private openToast(config: ToastConfig): Observable<void> {
    const { data, panelClass, translationKey } = config;

    return this.translateService.selectTranslate(translationKey).pipe(
      take(1),
      switchMap((message) =>
        this.snackBar
          .openFromComponent(CustomSnackbarComponent, {
            data: { ...data, message } satisfies CustomSnackbarData,
            panelClass,
            horizontalPosition: 'end',
            verticalPosition: 'top',
          })
          .afterDismissed()
      ),
      map(() => undefined)
    );
  }
}
