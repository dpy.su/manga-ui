import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import {
  MAT_SNACK_BAR_DATA,
  MatSnackBarAction,
  MatSnackBarActions,
  MatSnackBarLabel,
  MatSnackBarRef,
} from '@angular/material/snack-bar';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { filter, race, switchMap, tap, timer } from 'rxjs';
import { MatIconButton } from '@angular/material/button';
import { MatIcon } from '@angular/material/icon';
import { ActiveHoverDirective } from '@shared/ui';

export interface CustomSnackbarData {
  message: string;
  hideAfter: number;
  iconType: 'error' | 'info' | 'warning';
}

@Component({
  selector: 'moe-custom-snackbar',
  standalone: true,
  imports: [
    MatSnackBarLabel,
    MatSnackBarActions,
    MatSnackBarAction,
    MatIconButton,
    MatIcon,
  ],
  hostDirectives: [ActiveHoverDirective],
  templateUrl: './custom-snackbar.component.html',
  styleUrl: './custom-snackbar.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomSnackbarComponent {
  protected snackBarRef = inject(MatSnackBarRef);
  protected data: CustomSnackbarData = inject(MAT_SNACK_BAR_DATA);

  constructor() {
    const hover$ = inject(ActiveHoverDirective).hoverActive$;

    const closeOrHoverOn$ = race(
      timer(this.data.hideAfter).pipe(
        tap(() => this.snackBarRef.dismissWithAction())
      ),
      hover$.pipe(filter((x) => x))
    );

    hover$
      .pipe(
        filter((x) => !x),
        switchMap(() => closeOrHoverOn$),
        takeUntilDestroyed()
      )
      .subscribe();
  }
}
