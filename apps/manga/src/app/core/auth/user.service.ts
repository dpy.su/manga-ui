import { inject, Injectable } from '@angular/core';
import { map } from 'rxjs';
import { OidcSecurityService } from 'angular-auth-oidc-client';
import { toSignal } from '@angular/core/rxjs-interop';
import { filterNil } from 'ngxtension/filter-nil';

export type AuthUser = {
  id: string;
  username: string;
  email: string;
};

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private readonly oidcSecurityService = inject(OidcSecurityService);

  public readonly user = toSignal<AuthUser | null>(
    this.oidcSecurityService.userData$.pipe(
      map(x => x.userData),
      filterNil(),
      map(userData => ({
        id: userData.sub,
        email: userData.email,
        username: userData.preferred_username,
      }))
    ),
    { initialValue: null }
  );

  public readonly isAnonymous = toSignal(
    this.oidcSecurityService.isAuthenticated$.pipe(
      map(res => !res.isAuthenticated)
    ),
    { initialValue: true }
  );
}
