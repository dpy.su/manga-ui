import { CanActivateFn } from '@angular/router';
import { inject } from '@angular/core';
import { autoLoginPartialRoutesGuard } from 'angular-auth-oidc-client';
import { UserService } from '@core/auth/user.service';
import { PLATFORM_SSR } from '@core/injection-tokens';

export function requireAuthGuard(loginPrompt = true): CanActivateFn {
  return () => {
    const isSSR = inject(PLATFORM_SSR);
    if (isSSR) {
      return false;
    }

    if (loginPrompt) {
      return autoLoginPartialRoutesGuard();
    }

    const authService = inject(UserService);
    return !authService.isAnonymous();
  };
}
