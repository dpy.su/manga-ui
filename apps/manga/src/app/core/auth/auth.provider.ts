import {
  AbstractSecurityStorage,
  DefaultLocalStorageService,
  provideAuth,
  StsConfigLoader,
  StsConfigStaticLoader,
  withAppInitializerAuthCheck,
} from 'angular-auth-oidc-client';
import { inject, makeEnvironmentProviders } from '@angular/core';
import { environment } from '../../../environments/environment';
import { WINDOW } from '../injection-tokens';

const authFactory = () => {
  const window = inject(WINDOW);

  return new StsConfigStaticLoader({
    authority: environment.stsConfig.authority,
    redirectUrl: `${window.location.origin}/auth/sign-in-callback`,
    postLogoutRedirectUri: window.location.origin,
    secureRoutes: environment.stsConfig.secureRoutes,
    clientId: 'frontend',
    scope: 'openid profile offline_access',
    responseType: 'code',
    silentRenew: true,
    ignoreNonceAfterRefresh: true,
    useRefreshToken: true,
    renewTimeBeforeTokenExpiresInSeconds: 30,
  });
};

export const provideOIDCAuth = function () {
  return makeEnvironmentProviders([
    provideAuth(
      {
        loader: {
          provide: StsConfigLoader,
          useFactory: authFactory,
        },
      },
      withAppInitializerAuthCheck()
    ),
    {
      provide: AbstractSecurityStorage,
      useClass: DefaultLocalStorageService,
    },
  ]);
};
