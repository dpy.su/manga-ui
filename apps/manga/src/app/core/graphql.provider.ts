import {
  APOLLO_FLAGS,
  provideApollo,
} from 'apollo-angular';
import { HttpLink } from 'apollo-angular/http';
import {
  inject,
  InjectionToken,
  makeEnvironmentProviders,
  makeStateKey,
  TransferState,
} from '@angular/core';
import { InMemoryCache } from '@apollo/client/core';
import { environment } from '../../environments/environment';
import { PLATFORM_SSR } from './injection-tokens';

/* eslint-disable @typescript-eslint/no-explicit-any */

export const GRAPHQL_URL = new InjectionToken<string | null>('GRAPHQL_URL');
const STATE_KEY = makeStateKey<any>('apollo.state');

function populateCache(transferState: TransferState, cache: InMemoryCache) {
  const isBrowser = transferState.hasKey<any>(STATE_KEY);

  if (isBrowser) {
    const state = transferState.get<any>(STATE_KEY, null);
    cache.restore(state);
  } else {
    transferState.onSerialize(STATE_KEY, () => {
      return cache.extract();
    });
    // Reset cache after extraction to avoid sharing between requests
    cache.reset();
  }
}

export const provideGraphql = function () {
  return makeEnvironmentProviders([
    provideApollo(() => {
      const httpLink = inject(HttpLink);
      const uri = inject(GRAPHQL_URL, { optional: true }) ?? environment.graphqlUrl;

      const cache = new InMemoryCache();
      const transferState = inject(TransferState);
      populateCache(transferState, cache);

      const isSsr = inject(PLATFORM_SSR);

      return {
        link: httpLink.create({ uri }),
        cache,
        ssrMode: isSsr,
        devtools: {
          enabled: !environment.isProduction,
        },
        defaultOptions: {
          watchQuery: {
            fetchPolicy: 'cache-and-network',
          },
          query: {
            fetchPolicy: 'no-cache',
          },
        },
      };
    }),
    {
      provide: APOLLO_FLAGS,
      useValue: {
        useInitialLoading: true,
        useMutationLoading: true,
      },
    },
  ]);
};
