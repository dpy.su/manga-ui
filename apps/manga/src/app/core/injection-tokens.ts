import {
  ApplicationRef,
  inject,
  InjectionToken,
  PLATFORM_ID,
} from '@angular/core';
import { DOCUMENT, isPlatformServer } from '@angular/common';
import { filter, Observable, take } from 'rxjs';

export const WINDOW = new InjectionToken<Window>(
  'An abstraction over global window object',
  {
    factory: () => {
      const { defaultView } = inject(DOCUMENT);
      if (!defaultView) {
        throw new Error('Window is not available');
      }

      return defaultView;
    },
  }
);

export const APP_STABLE = new InjectionToken<Observable<boolean>>(
  'Observable of ApplicationRef.isStable. Will emit only once when true',
  {
    factory: () => {
      return inject(ApplicationRef).isStable.pipe(
        filter((x) => x),
        take(1)
      );
    },
  }
);

export const PLATFORM_SSR = new InjectionToken<boolean>(
  'Indicates if app is rendering in ssr mode',
  {
    factory: () => isPlatformServer(inject(PLATFORM_ID)),
  }
);
