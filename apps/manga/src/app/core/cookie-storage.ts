import Cookies from 'universal-cookie';
import { inject, InjectionToken, REQUEST } from '@angular/core';

interface CookieOptions {
  path?: string;
  expires?: Date;
  maxAge?: number;
  domain?: string;
  secure?: boolean;
  httpOnly?: boolean;
  sameSite?: boolean | 'none' | 'lax' | 'strict';
  partitioned?: boolean;
}

interface CookieGetOptions {
  doNotParse?: boolean;
  doNotUpdate?: boolean;
}

export interface CookieStorage {
  get<T>(name: string, options?: CookieGetOptions): T | null;

  set(name: string, value: string | object, options?: CookieOptions): void;
}

export const COOKIE_STORAGE = new InjectionToken<CookieStorage>(
  'An abstraction over cookies',
  {
    factory: () => new UniversalCookieStorage(),
  }
);

class UniversalCookieStorage implements CookieStorage {
  private request = inject(REQUEST, { optional: true });
  private cookies = new Cookies(this.request?.headers.get('cookie'));

  public get<T>(name: string, options?: CookieGetOptions): T {
    return this.cookies.get<T>(name, options);
  }

  public set(
    name: string,
    value: string | object,
    options?: CookieOptions
  ): void {
    this.cookies.set(name, value, options);
  }
}
