import { inject, Injectable } from '@angular/core';
import { COOKIE_STORAGE } from '../cookie-storage';
import { Theme } from './theme.service';

const THEME_KEY = 'settings.theme';

@Injectable({
  providedIn: 'root',
})
export class ThemeStorage {
  private storage = inject(COOKIE_STORAGE);

  get(): Theme | null {
    const theme = this.storage.get<Theme | null>(THEME_KEY);
    return theme?.length === 0 ? null : theme;
  }

  set(theme: Theme): void {
    return this.storage.set(THEME_KEY, theme);
  }
}
