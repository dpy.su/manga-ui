import {
  Inject,
  Injectable,
  signal,
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { ThemeStorage } from './theme-storage.service';

export type Theme = 'light' | 'dark';

@Injectable({
  providedIn: 'root',
})
export class ThemeService {
  public readonly availableThemes: readonly Theme[] = ['dark', 'light'];

  private readonly _theme = signal<Theme>('dark');
  public readonly theme = this._theme.asReadonly();

  constructor(
    @Inject(DOCUMENT) private readonly document: Document,
    private readonly themeStorage: ThemeStorage
  ) {}

  public toggle(): void {
    if (this._theme() == 'light') {
      this.setDark();
    } else {
      this.setLight();
    }
  }

  public setLight(): void {
    this.themeStorage.set('light');
    this._theme.set('light');
    this.document.body.classList.add('light-theme');
  }

  public setDark(): void {
    this.themeStorage.set('dark');
    this._theme.set('dark');
    this.document.body.classList.remove('light-theme');
  }

  public initPersistedTheme(): void {
    const theme = this.themeStorage.get();
    if (theme == null) {
      return;
    }

    if (theme == 'light') {
      this.setLight();
    } else {
      this.setDark();
    }
  }
}
