import {
  APP_INITIALIZER,
  inject,
  makeEnvironmentProviders,
} from '@angular/core';
import { ThemeService } from './theme.service';

function initializeTheme() {
  const theme = inject(ThemeService);

  return () => theme.initPersistedTheme();
}

export const provideTheming = function () {
  return makeEnvironmentProviders([
    {
      provide: APP_INITIALIZER,
      useFactory: initializeTheme,
      multi: true,
    },
  ]);
};
