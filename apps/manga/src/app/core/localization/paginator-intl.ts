import { MatPaginatorIntl } from '@angular/material/paginator';
import { Subject } from 'rxjs';
import { inject } from '@angular/core';
import { TranslocoService } from '@jsverse/transloco';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

export class PaginatorIntl implements MatPaginatorIntl {
  changes = new Subject<void>();

  private _translateService = inject(TranslocoService);

  firstPageLabel = '';
  itemsPerPageLabel = '';
  lastPageLabel = '';
  nextPageLabel = '';
  previousPageLabel = '';

  getRangeLabel(page: number, pageSize: number, length: number): string {
    // TODO: fix missing translation in SSR
    if (length === 0) {
      return this._translateService.translate(
        'COMPONENTS.PAGINATOR.PAGE_X_OF_Y',
        { x: page + 1, y: 1 }
      );
    }
    const amountPages = Math.ceil(length / pageSize);
    return this._translateService.translate(
      'COMPONENTS.PAGINATOR.PAGE_X_OF_Y',
      {
        x: page + 1,
        y: amountPages,
      }
    );
  }

  constructor() {
    this._translateService
      .selectTranslateObject('COMPONENTS.PAGINATOR')
      .pipe(takeUntilDestroyed())
      .subscribe((x) => {
        this.firstPageLabel = x['FIRST_PAGE'];
        this.itemsPerPageLabel = x['ITEMS_PER_PAGE'];
        this.lastPageLabel = x['LAST_PAGE'];
        this.nextPageLabel = x['NEXT_PAGE'];
        this.previousPageLabel = x['PREVIOUS_PAGE'];
      });
  }
}
