import { inject, Injectable, InjectionToken, signal } from '@angular/core';
import { TranslocoService } from '@jsverse/transloco';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { tap } from 'rxjs';
import { LanguageStorage } from './language-storage.service';
import { LanguageEnum } from '@generated-graphql';

export interface LanguageInfo {
  code: string;
  languageEnum: LanguageEnum;
  fullName: string;
}

export type AvailableLanguages = ReadonlyMap<string, LanguageInfo>;

export const AVAILABLE_LANGUAGES = new InjectionToken<AvailableLanguages>(
  'Available languages'
);
export const PREFERRED_LANGUAGE = new InjectionToken<string | null>(
  'Preferred language'
);

@Injectable({
  providedIn: 'root',
})
export class LanguageService {
  public readonly availableLanguages = inject(AVAILABLE_LANGUAGES);

  private translateService = inject(TranslocoService);
  private languageStorage = inject(LanguageStorage);

  private preferredLanguage = inject(PREFERRED_LANGUAGE, { optional: true });

  private readonly _currentLanguage = signal(
    this.languageStorage.get() ?? this.translateService.getDefaultLang()
  );
  public readonly currentLanguage = this._currentLanguage.asReadonly();

  constructor() {
    this.translateService.langChanges$
      .pipe(
        tap((lang) => {
          this._currentLanguage.set(lang);
          this.languageStorage.set(lang);
        }),
        takeUntilDestroyed()
      )
      .subscribe();
  }

  public setCurrentLanguage(lang: string) {
    if (this.availableLanguages.get(lang) == null) {
      throw new Error(`Language doesn't exist`);
    }
    this.translateService.setActiveLang(lang);
  }

  public initPersistedLang(): void {
    const lang = this.languageStorage.get();

    if (lang != null) {
      this.setCurrentLanguage(lang);
      return;
    }

    if (this.preferredLanguage == null) {
      return;
    }

    const preferredLanguageSupported = this.availableLanguages.has(
      this.preferredLanguage
    );
    if (!preferredLanguageSupported) {
      return;
    }
    this.setCurrentLanguage(this.preferredLanguage);
  }
}
