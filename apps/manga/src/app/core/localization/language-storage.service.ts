import { inject, Injectable } from '@angular/core';
import { COOKIE_STORAGE } from '../cookie-storage';

const LANGUAGE_KEY = 'settings.language';

@Injectable({
  providedIn: 'root',
})
export class LanguageStorage {
  private storage = inject(COOKIE_STORAGE);

  get(): string | null {
    return this.storage.get<string | null>(LANGUAGE_KEY);
  }

  set(lang: string): void {
    this.storage.set(LANGUAGE_KEY, lang);
  }
}
