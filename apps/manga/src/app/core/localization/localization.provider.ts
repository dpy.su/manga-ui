import {
  inject,
  makeEnvironmentProviders,
  provideAppInitializer,
} from '@angular/core';
import { provideTransloco } from '@jsverse/transloco';
import { TitleStrategy } from '@angular/router';
import { AVAILABLE_LANGUAGES, LanguageService } from './language.service';
import { LocalizedTitleStrategy } from './localized-title-strategy';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { PaginatorIntl } from './paginator-intl';
import { LanguageEnum } from '@generated-graphql';
import { environment } from '../../../environments/environment';
import { TranslocoHttpLoader } from '@core/localization/transloco-http-loader.service';

function initializeLocale() {
  const lang = inject(LanguageService);

  return lang.initPersistedLang();
}

export const provideLocalization = function () {
  return makeEnvironmentProviders([
    provideTransloco({
      config: {
        availableLangs: ['eng', 'ukr'],
        defaultLang: 'eng',
        reRenderOnLangChange: true,
        prodMode: environment.isProduction,
      },
      loader: TranslocoHttpLoader,
    }),
    { provide: TitleStrategy, useClass: LocalizedTitleStrategy },
    provideAppInitializer(initializeLocale),
    {
      provide: AVAILABLE_LANGUAGES,
      useValue: new Map([
        [
          'eng',
          { code: 'eng', languageEnum: LanguageEnum.Eng, fullName: 'English' },
        ],
        [
          'ukr',
          {
            code: 'ukr',
            languageEnum: LanguageEnum.Ukr,
            fullName: 'Українська',
          },
        ],
      ]),
    },
    {
      provide: MatPaginatorIntl,
      useClass: PaginatorIntl,
    },
  ]);
};
