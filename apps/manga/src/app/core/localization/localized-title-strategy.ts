import { RouterStateSnapshot, TitleStrategy } from '@angular/router';
import { inject, Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { TranslocoService } from '@jsverse/transloco';
import { take } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class LocalizedTitleStrategy extends TitleStrategy {
  private title = inject(Title);
  private translateService = inject(TranslocoService);

  public updateTitle(snapshot: RouterStateSnapshot): void {
    const titleKey = this.buildTitle(snapshot);
    if (titleKey == null) {
      this.title.setTitle('MoeManga');
      return;
    }

    this.translateService
      .selectTranslate(titleKey)
      .pipe(take(1))
      .subscribe((title) => this.title.setTitle(`${title} | MoeManga`));
  }
}
