import { Pipe, PipeTransform } from '@angular/core';
import { orderBy } from 'lodash-es';

@Pipe({
  name: 'orderBy',
})
export class OrderByPipe implements PipeTransform {
  public transform<T>(
    collection: T[],
    property: keyof T,
    order?: 'asc' | 'desc'
  ): T[] {
    return orderBy(collection, [property], order);
  }
}
