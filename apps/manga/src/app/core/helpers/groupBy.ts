export function groupBy<TItem, TKey>(
  items: TItem[],
  keySelector: (item: TItem) => TKey
): Map<TKey, TItem[]> {
  const groups = new Map<TKey, TItem[]>();

  for (const item of items) {
    const key = keySelector(item);
    const group = groups.get(key);
    if (group != null) {
      group.push(item);
    } else {
      groups.set(key, [item]);
    }
  }

  return groups;
}
