import { ApplicationConfig, mergeApplicationConfig } from '@angular/core';
import { provideServerRendering } from '@angular/platform-server';
import { provideServerRoutesConfig } from '@angular/ssr';
import { appConfig } from './app.config';
import { serverRoutes } from './app.routes.server';
import { GRAPHQL_URL } from '@core/graphql.provider';

const serverConfig: ApplicationConfig = {
  providers: [
    provideServerRendering(),
    provideServerRoutesConfig(serverRoutes),
    { provide: GRAPHQL_URL, useValue: process.env['GRAPHQL_SSR_URL'] ?? null },
  ],
};

export const config = mergeApplicationConfig(appConfig, serverConfig);
