import {
  ChangeDetectionStrategy,
  Component,
  computed,
  input,
} from '@angular/core';
import { MangaStatus } from '@generated-graphql';
import { CommonModule } from '@angular/common';
import { TranslocoDirective } from '@jsverse/transloco';

const BADGE_CLASS = 'manga-status__badge';

@Component({
  selector: 'moe-manga-status',
  standalone: true,
  imports: [CommonModule, TranslocoDirective],
  templateUrl: './manga-status.component.html',
  styleUrl: './manga-status.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MangaStatusComponent {
  public status = input.required<MangaStatus>();

  protected statusIconClass = computed(() => {
    switch (this.status()) {
      case MangaStatus.Cancelled:
        return `${BADGE_CLASS}--cancelled`;
      case MangaStatus.Completed:
        return `${BADGE_CLASS}--completed`;
      case MangaStatus.Ongoing:
        return `${BADGE_CLASS}--ongoing`;
      case MangaStatus.OnHold:
        return `${BADGE_CLASS}--onhold`;
    }
  });
}
