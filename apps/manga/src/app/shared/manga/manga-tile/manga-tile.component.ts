import { ChangeDetectionStrategy, Component, input } from '@angular/core';
import { MangaTileFragment } from '@generated-graphql';

import { MatCardModule } from '@angular/material/card';
import { MangaStatsComponent } from '../manga-stats/manga-stats.component';
import { MangaStatusComponent } from '../manga-status/manga-status.component';
import { MangaTagsComponent } from '../manga-tags/manga-tags.component';
import { MangaCoverComponent } from '../manga-cover/manga-cover.component';
import { MangaTitleComponent } from '@shared/manga/manga-title/manga-title.component';

export type ItemAppearance = 'card' | 'cover';

@Component({
  selector: 'moe-manga-tile',
  standalone: true,
  imports: [
    MatCardModule,
    MangaStatsComponent,
    MangaStatusComponent,
    MangaTagsComponent,
    MangaCoverComponent,
    MangaTitleComponent,
  ],
  templateUrl: './manga-tile.component.html',
  styleUrl: './manga-tile.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MangaTileComponent {
  public manga = input.required<MangaTileFragment>();
  public appearance = input<ItemAppearance>('card');
}
