import {
  ChangeDetectionStrategy,
  Component,
  computed,
  contentChild,
  Directive,
  input,
  model,
  TemplateRef,
} from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MangaTileFragment } from '@generated-graphql';
import { NgClass, NgTemplateOutlet } from '@angular/common';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import { ItemAppearance, MangaTileComponent } from '@shared/manga';
import { TranslocoDirective } from '@jsverse/transloco';

export type MangaGridAppearance = 'card' | 'cover';

interface GridVariant {
  type: MangaGridAppearance;
  toggleIcon: string;
  itemAppearance: ItemAppearance;
  class: string;
}

const GRID_VARIANTS: readonly GridVariant[] = [
  {
    type: 'card',
    toggleIcon: 'view_agenda',
    itemAppearance: 'card',
    class: 'manga-grid-list--cards',
  },
  {
    type: 'cover',
    toggleIcon: 'grid_view',
    itemAppearance: 'cover',
    class: 'manga-grid-list--covers',
  },
];

@Directive({
  /* eslint-disable-next-line @angular-eslint/directive-selector */
  selector: 'ng-template[mangaGridEmptyPlaceholder]',
  standalone: true,
})
/* eslint-disable-next-line @angular-eslint/directive-class-suffix */
export class MangaGridEmptyPlaceholder {}

@Component({
  selector: 'moe-manga-grid-list',
  standalone: true,
  imports: [
    NgTemplateOutlet,
    ReactiveFormsModule,
    NgClass,
    MatCardModule,
    MatButtonToggleModule,
    MatIconModule,
    MangaTileComponent,
    TranslocoDirective,
  ],
  templateUrl: './manga-grid-list.component.html',
  styleUrl: './manga-grid-list.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MangaGridListComponent {
  public manga = input<MangaTileFragment[]>([]);
  public appearance = model<MangaGridAppearance>('card');

  protected emptyPlaceholder = contentChild(MangaGridEmptyPlaceholder, {
    read: TemplateRef,
  });
  protected showEmptyPlaceHolder = computed(() => this.manga().length === 0);

  protected gridVariants = GRID_VARIANTS;

  protected gridVariant = computed(() =>
    this.getVariantByAppearance(this.appearance())
  );

  private getVariantByAppearance(appearance: MangaGridAppearance): GridVariant {
    const variant = GRID_VARIANTS.find((x) => x.itemAppearance == appearance);
    if (variant == null) {
      throw new Error('Variant not found');
    }

    return variant;
  }
}
