import {
  ChangeDetectionStrategy,
  Component,
  computed,
  input,
  signal,
} from '@angular/core';
import { MangaTagFragment } from '@generated-graphql';
import { TranslocoDirective } from '@jsverse/transloco';

@Component({
  selector: 'moe-manga-tags',
  standalone: true,
  imports: [TranslocoDirective],
  templateUrl: './manga-tags.component.html',
  styleUrl: './manga-tags.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MangaTagsComponent {
  public tags = input.required<MangaTagFragment[]>();

  protected previewTags = computed(() => this.tags().slice(0, 5));

  private tagsCollapsed = signal(true);

  protected displayShowMore = computed(
    () => this.tagsCollapsed() && this.tags().length > 5
  );

  protected showAllTags() {
    this.tagsCollapsed.set(false);
  }
}
