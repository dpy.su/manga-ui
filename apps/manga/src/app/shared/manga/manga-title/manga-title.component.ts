import { ChangeDetectionStrategy, Component, input } from '@angular/core';
import { MangaTitleFragment } from '@generated-graphql';
import { RouterLink } from '@angular/router';
import { LanguageIconComponent } from '@shared/ui';

@Component({
  selector: 'moe-manga-title',
  standalone: true,
  imports: [RouterLink, LanguageIconComponent, LanguageIconComponent],
  templateUrl: './manga-title.component.html',
  styleUrl: './manga-title.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MangaTitleComponent {
  public manga = input.required<MangaTitleFragment>();
}
