import {
  ChangeDetectionStrategy,
  Component,
  computed,
  inject,
  input,
} from '@angular/core';
import { Image, MangaCoverFragment } from '@generated-graphql';
import {
  IMAGE_LOADER,
  ImageLoaderConfig,
  NgOptimizedImage,
} from '@angular/common';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { PLATFORM_SSR } from '@core/injection-tokens';
import { RouterLink } from '@angular/router';
import { minBy } from 'lodash-es';

type CustomLoaderParams = { images: Pick<Image, 'width' | 'url'>[] };
type CustomImageLoaderConfig = ImageLoaderConfig & {
  loaderParams?: CustomLoaderParams;
};

@Component({
  selector: 'moe-manga-cover',
  standalone: true,
  imports: [NgOptimizedImage, RouterLink],
  providers: [
    {
      provide: IMAGE_LOADER,
      useValue: (config: CustomImageLoaderConfig) => {
        if (!config.loaderParams) {
          return config.src;
        }

        const appropriateImages = config.loaderParams.images.filter(
          (img) => img.width >= (config.width ?? 0)
        );
        // Grab smallest appropriate image, or closest image
        const image =
          minBy(appropriateImages, (img) => img.width) ??
          minBy(config.loaderParams.images, (img) =>
            Math.abs((config.width ?? 0) - img.width)
          );
        return image?.url ?? config.src;
      },
    },
  ],
  templateUrl: './manga-cover.component.html',
  styleUrl: './manga-cover.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MangaCoverComponent {
  public priorityLoadingEnabled = !inject(PLATFORM_SSR);

  public manga = input.required<MangaCoverFragment>();
  public sizes = input.required<string>();
  public fillHeight = input(false, { transform: coerceBooleanProperty });

  protected srcSet = computed<string>(
    () =>
      this.manga()
        .coverArt?.image.alternatives.map((x) => `${x.width}w`)
        .join(', ') ?? ''
  );
}
