import { ChangeDetectionStrategy, Component, input } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MangaStatsFragment } from '@generated-graphql';
import { CommonModule } from '@angular/common';
import { ShortNumberPipe } from '@shared/ui';

@Component({
  selector: 'moe-manga-stats',
  standalone: true,
  imports: [CommonModule, MatIconModule, ShortNumberPipe, ShortNumberPipe],
  templateUrl: './manga-stats.component.html',
  styleUrl: './manga-stats.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MangaStatsComponent {
  public manga = input.required<MangaStatsFragment>();
}
