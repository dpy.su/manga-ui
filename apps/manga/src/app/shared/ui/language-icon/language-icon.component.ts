import {
  ChangeDetectionStrategy,
  Component,
  computed,
  input,
} from '@angular/core';
import { MatIcon } from '@angular/material/icon';
import { LanguageEnum } from '@generated-graphql';

const FALLBACK_FLAG_ICON = 'flags:fallback';

const FLAG_ICONS: ReadonlyMap<LanguageEnum, string> = new Map([
  [LanguageEnum.Eng, 'flags:eng'],
  [LanguageEnum.Ukr, 'flags:ukr'],
  [LanguageEnum.Jpn, 'flags:jpn'],
]);

@Component({
  selector: 'moe-language-icon',
  imports: [MatIcon],
  templateUrl: './language-icon.component.html',
  styleUrl: './language-icon.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LanguageIconComponent {
  public language = input.required<LanguageEnum>();
  protected flagIcon = computed(
    () => FLAG_ICONS.get(this.language()) ?? FALLBACK_FLAG_ICON
  );
}
