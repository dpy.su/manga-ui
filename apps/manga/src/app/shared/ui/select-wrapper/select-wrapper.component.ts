import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  contentChild,
  ElementRef,
  HostBinding,
  HostListener,
  inject,
  Input,
  numberAttribute,
  OnDestroy,
} from '@angular/core';
import {
  CdkOverlayOrigin,
  ConnectedPosition,
  OverlayModule,
  ViewportRuler,
} from '@angular/cdk/overlay';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { Observable, Subject } from 'rxjs';
import { MatIconModule } from '@angular/material/icon';
import { BooleanInput, coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  MAT_FORM_FIELD,
  MatFormFieldControl,
} from '@angular/material/form-field';
import { NgxControlValueAccessor } from 'ngxtension/control-value-accessor';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { AsyncPipe } from '@angular/common';
import { hasModifierKey } from '@angular/cdk/keycodes';

export abstract class CustomSelectInputControl<T> {
  abstract readonly cva: NgxControlValueAccessor<T>;
  abstract readonly triggerValue: Observable<string>;

  abstract get isEmpty(): boolean;
}

@Component({
  selector: 'moe-select-wrapper',
  standalone: true,
  imports: [OverlayModule, MatIconModule, AsyncPipe],
  hostDirectives: [NgxControlValueAccessor],
  providers: [
    { provide: MatFormFieldControl, useExisting: SelectWrapperComponent },
  ],
  host: {
    '[attr.tabindex]': 'disabled ? -1 : tabIndex',
  },
  templateUrl: './select-wrapper.component.html',
  styleUrl: './select-wrapper.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectWrapperComponent<T>
  implements MatFormFieldControl<T>, OnDestroy
{
  private static nextId = 0;

  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input('aria-describedby')
  public userAriaDescribedBy = '';

  @HostBinding()
  public id = `select-input-${SelectWrapperComponent.nextId++}`;

  public ngControl = null;

  public controlType = 'select-wrapper';

  public stateChanges = new Subject<void>();

  protected preferredOverlayOrigin?: CdkOverlayOrigin | ElementRef;

  protected overlayWidth: string | number = '';

  protected positions: ConnectedPosition[] = [
    {
      originX: 'start',
      originY: 'bottom',
      overlayX: 'start',
      overlayY: 'top',
    },
    {
      originX: 'end',
      originY: 'bottom',
      overlayX: 'end',
      overlayY: 'top',
    },
    {
      originX: 'start',
      originY: 'top',
      overlayX: 'start',
      overlayY: 'bottom',
    },
    {
      originX: 'end',
      originY: 'top',
      overlayX: 'end',
      overlayY: 'bottom',
    },
  ];

  private readonly _cdr = inject(ChangeDetectorRef);
  private readonly _elementRef = inject(ElementRef);
  private readonly _breakpointObserver = inject(BreakpointObserver);
  private readonly _viewportRuler = inject(ViewportRuler);

  private readonly _parentFormField = inject(MAT_FORM_FIELD, {
    optional: true,
  });

  private readonly _customControl = contentChild.required(
    CustomSelectInputControl<T>
  );

  constructor() {
    this._viewportRuler
      .change()
      .pipe(takeUntilDestroyed())
      .subscribe(() => {
        if (this.panelOpen) {
          this.overlayWidth = this._getOverlayWidth(
            this.preferredOverlayOrigin
          );
          this._cdr.detectChanges();
        }
      });
  }

  private _placeholder = '';

  @Input()
  public get placeholder() {
    return this._placeholder;
  }

  public set placeholder(plh) {
    this._placeholder = plh;
    this.stateChanges.next();
  }

  @Input()
  public get disabled(): boolean {
    return this._customControl().cva.disabled;
  }

  public set disabled(value: BooleanInput) {
    const disabled = coerceBooleanProperty(value);
    this._customControl().cva.setDisabledState(disabled);
    this.stateChanges.next();
  }

  private _required = false;

  @Input()
  public get required(): boolean {
    return this._required;
  }

  public set required(req: BooleanInput) {
    this._required = coerceBooleanProperty(req);
    this.stateChanges.next();
  }

  @Input({
    transform: (value: unknown) => (value == null ? 0 : numberAttribute(value)),
  })
  public tabIndex = 0;

  public get value(): T {
    return this._customControl().cva.value;
  }

  public set value(val: T) {
    this._customControl().cva.value = val;
    this.stateChanges.next();
  }

  private _focused = false;

  public get focused(): boolean {
    return this._focused || this._panelOpen;
  }

  public get errorState(): boolean {
    return false;
  }

  public get empty() {
    return this._customControl().isEmpty;
  }

  public get shouldLabelFloat(): boolean {
    return (
      this.panelOpen || !this.empty || (this.focused && !!this.placeholder)
    );
  }

  protected get triggerValue() {
    return this._customControl().triggerValue;
  }

  private _panelOpen = false;

  protected get panelOpen(): boolean {
    return this._panelOpen;
  }

  public ngOnDestroy() {
    this.stateChanges.complete();
  }

  public onContainerClick() {
    this._elementRef.nativeElement.focus();
    this.open();
  }

  public setDescribedByIds(ids: string[]) {
    if (ids.length) {
      this._elementRef.nativeElement.setAttribute(
        'aria-describedby',
        ids.join(' ')
      );
    } else {
      this._elementRef.nativeElement.removeAttribute('aria-describedby');
    }
  }

  protected open() {
    if (!this._canOpen()) {
      return;
    }

    if (this._parentFormField) {
      this.preferredOverlayOrigin =
        this._parentFormField.getConnectedOverlayOrigin();
    }

    this.overlayWidth = this._getOverlayWidth(this.preferredOverlayOrigin);
    this._panelOpen = true;

    this._cdr.markForCheck();
    this.stateChanges.next();
  }

  protected close() {
    if (this._panelOpen) {
      this._panelOpen = false;

      this._cdr.markForCheck();
      this.stateChanges.next();
    }
  }

  @HostListener('focus')
  protected onFocus() {
    if (!this.disabled) {
      this._focused = true;
      this.stateChanges.next();
    }
  }

  @HostListener('blur')
  protected onBlur() {
    this._focused = false;

    if (!this.disabled && !this.panelOpen) {
      this._customControl().cva.markAsTouched();
      this._cdr.markForCheck();
      this.stateChanges.next();
    }
  }

  @HostListener('keydown', ['$event'])
  protected handleKeydown(event: KeyboardEvent): void {
    if (!this.disabled) {
      return;
    }

    if (this.panelOpen) {
      this._handleCloseKeydown(event);
      return;
    }

    this._handleOpenKeydown(event);
  }

  private _handleOpenKeydown(event: KeyboardEvent): void {
    const keyCode = event.code;
    if (
      (keyCode === 'Enter' || keyCode === 'Space') &&
      !hasModifierKey(event)
    ) {
      event.preventDefault();
      this.open();
    }
  }

  private _handleCloseKeydown(event: KeyboardEvent): void {
    const keyCode = event.code;

    if (keyCode === 'Escape' && !hasModifierKey(event)) {
      event.preventDefault();
      this.close();
    }
  }

  private _canOpen(): boolean {
    return !this._panelOpen && !this.disabled;
  }

  private _getOverlayWidth(
    preferredOrigin?: ElementRef | CdkOverlayOrigin
  ): string | number {
    const isMobile = this._breakpointObserver.isMatched(Breakpoints.XSmall);
    if (isMobile) {
      const refToMeasure =
        preferredOrigin instanceof CdkOverlayOrigin
          ? preferredOrigin.elementRef
          : preferredOrigin || this._elementRef;
      return refToMeasure.nativeElement.getBoundingClientRect().width;
    }

    const isSmall = this._breakpointObserver.isMatched(Breakpoints.Small);
    if (isSmall) {
      return '470px';
    }

    const isMedium = this._breakpointObserver.isMatched(Breakpoints.Medium);
    if (isMedium) {
      return '670px';
    }

    const isLarge = this._breakpointObserver.isMatched(Breakpoints.Large);
    if (isLarge) {
      return '800px';
    }

    return '1000px';
  }
}
