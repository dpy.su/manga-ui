import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { Location } from '@angular/common';

@Component({
  selector: 'moe-navigate-back-btn',
  standalone: true,
  imports: [MatIconModule, MatButtonModule],
  templateUrl: './navigate-back-btn.component.html',
  styleUrl: './navigate-back-btn.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavigateBackBtnComponent {
  private location = inject(Location);

  protected onClick() {
    this.location.back();
  }
}
