import {
  ChangeDetectionStrategy,
  Component,
  inject,
  input,
} from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import {
  NgxControlValueAccessor,
  provideCvaCompareTo,
} from 'ngxtension/control-value-accessor';

@Component({
  selector: 'moe-search-input',
  hostDirectives: [NgxControlValueAccessor],
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    ReactiveFormsModule,
  ],
  providers: [provideCvaCompareTo((a, b) => a === b, true)],
  templateUrl: './search-input.component.html',
  styleUrl: './search-input.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchInputComponent {
  public placeholder = input('');

  protected cva = inject<NgxControlValueAccessor<string>>(
    NgxControlValueAccessor
  );
}
