import { inject, Pipe, PipeTransform } from '@angular/core';
import { TranslocoService } from '@jsverse/transloco';

const NUMBER_SUFFIXES = ['K', 'M', 'B', 'T', 'Q', 'E'];

@Pipe({
  name: 'shortNumber',
  standalone: true,
})
export class ShortNumberPipe implements PipeTransform {
  private translateService = inject(TranslocoService);

  transform(input: number): string | number | null {
    if (Number.isNaN(input)) {
      return null;
    }

    if (Math.abs(input) < 1000) {
      return input;
    }

    const exp = Math.floor(Math.log(input) / Math.log(1000));

    const suffix = NUMBER_SUFFIXES[exp - 1];
    const localizedSuffix = this.translateService.translate(
      `COMMON.NUMBER_SHORT_SUFFIXES.${suffix}`
    );

    return (input / Math.pow(1000, exp)).toFixed() + localizedSuffix;
  }
}
