import { Directive, ElementRef, inject, output } from '@angular/core';
import {
  outputToObservable,
  takeUntilDestroyed,
  toSignal,
} from '@angular/core/rxjs-interop';
import { fromEvent, map, merge, startWith } from 'rxjs';

@Directive({
  selector: '[moeActiveHover]',
  standalone: true,
})
export class ActiveHoverDirective {
  public readonly hoverChange = output<boolean>();

  private readonly _hoverObservable = outputToObservable(this.hoverChange);
  public readonly hover = toSignal(this._hoverObservable, {
    initialValue: false,
  });
  public readonly hoverActive$ = this._hoverObservable.pipe(startWith(false));

  constructor() {
    const element = inject(ElementRef).nativeElement;
    const mouseEnter$ = fromEvent(element, 'mouseenter').pipe(map(() => true));
    const mouseLeave$ = fromEvent(element, 'mouseleave').pipe(map(() => false));

    merge(mouseEnter$, mouseLeave$)
      .pipe(takeUntilDestroyed())
      .subscribe((val) => this.hoverChange.emit(val));
  }
}
