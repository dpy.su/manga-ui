import {
  ChangeDetectionStrategy,
  Component,
  computed,
  input,
} from '@angular/core';
import { CommonModule } from '@angular/common';

type SkeletonShape = 'Rectangle' | 'Circle';

@Component({
  selector: 'moe-skeleton',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './skeleton.component.html',
  styleUrl: './skeleton.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SkeletonComponent {
  public shape = input<SkeletonShape>('Rectangle');

  public width = input('100%');
  public height = input('1rem');

  protected style = computed(() => {
    return {
      width: this.width(),
      height: this.height(),
    };
  });

  protected class = computed(() => {
    return {
      'skeleton--circle': this.shape() == 'Circle',
    };
  });
}
