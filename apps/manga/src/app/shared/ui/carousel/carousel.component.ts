import {
  afterNextRender,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  computed,
  contentChildren,
  Directive,
  ElementRef,
  HostBinding,
  inject,
  Injector,
  input,
  signal,
  viewChild,
} from '@angular/core';
import { FocusableOption, FocusKeyManager } from '@angular/cdk/a11y';
import { MatIcon } from '@angular/material/icon';
import { MatIconButton } from '@angular/material/button';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { takeUntilDestroyed, toObservable } from '@angular/core/rxjs-interop';
import { EMPTY, interval, switchMap, tap, withLatestFrom } from 'rxjs';
import { APP_STABLE } from '@core/injection-tokens';
import { ActiveHoverDirective } from '../active-hover.directive';

@Directive({
  selector: '[moeCarouselItem]',
  standalone: true,
})
export class CarouselItemDirective implements FocusableOption {
  public readonly element = inject(ElementRef<HTMLElement>);

  @HostBinding('attr.role')
  private readonly role = 'listitem';

  @HostBinding('tabindex')
  public tabindex = '-1';

  public focus(): void {
    this.element.nativeElement.focus({ preventScroll: true });
  }
}

interface AutoScrollConfig {
  delay: number;
  pauseOnHover: boolean;
}

const DEFAULT_AUTOSCROLL_CONFIG: AutoScrollConfig = {
  delay: 7000,
  pauseOnHover: true,
};

@Component({
  selector: 'moe-carousel',
  standalone: true,
  imports: [MatIcon, MatIconButton],
  hostDirectives: [ActiveHoverDirective],
  templateUrl: './carousel.component.html',
  styleUrl: './carousel.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CarouselComponent {
  public autoScroll = input(true, { transform: coerceBooleanProperty });
  public autoScrollConfig = input(DEFAULT_AUTOSCROLL_CONFIG);

  public disabled = input(false, { transform: coerceBooleanProperty });

  private hasNext = signal(true);
  protected nextDisabled = computed(() => !this.hasNext() || this.disabled());

  private hasPrevious = signal(false);
  protected previousDisabled = computed(
    () => !this.hasPrevious() || this.disabled()
  );

  private index = signal(0);
  private position = signal(0);

  private list = viewChild.required<ElementRef<HTMLElement>>('list');
  private items = contentChildren(CarouselItemDirective);

  private keyManager = new FocusKeyManager<CarouselItemDirective>(
    this.items,
    inject(Injector)
  );

  private cdr = inject(ChangeDetectorRef);

  constructor() {
    const hoverActive$ = inject(ActiveHoverDirective).hoverActive$;
    const disabled$ = toObservable(this.disabled);

    const scrollInterval$ = toObservable(this.autoScrollConfig).pipe(
      switchMap((cfg) => interval(cfg.delay)),
      withLatestFrom(hoverActive$, disabled$),
      tap(([, hoverActive, disabled]) =>
        this.autoScrollTick(disabled || hoverActive)
      )
    );
    const autoScrollEnabled$ = toObservable(this.autoScroll);

    const autoScroll$ = inject(APP_STABLE).pipe(
      switchMap(() => autoScrollEnabled$),
      switchMap((val) => (val ? scrollInterval$ : EMPTY)),
      takeUntilDestroyed()
    );

    afterNextRender(() => autoScroll$.subscribe());
  }

  private autoScrollTick(disabled: boolean) {
    if (disabled) {
      return;
    }

    this.scrollToNextItem();
    this.cdr.detectChanges();
  }

  private scrollToNextItem() {
    if (!this.hasNext() && !this.hasPrevious()) {
      return;
    }

    if (this.hasNext()) {
      this.next();
      return;
    }

    this.index.set(0);
    this.scrollToActiveItem();
  }

  protected next() {
    for (let i = this.index(); i < this.items().length; i++) {
      if (this.isOutOfView(i)) {
        this.index.set(i);
        this.scrollToActiveItem();
        break;
      }
    }
  }

  protected previous() {
    for (let i = this.index(); i > -1; i--) {
      if (this.isOutOfView(i)) {
        this.index.set(i);
        this.scrollToActiveItem();
        break;
      }
    }
  }

  protected onKeydown({ code }: KeyboardEvent) {
    const manager = this.keyManager;
    const previousActiveIndex = manager.activeItemIndex;

    if (code === 'ArrowLeft') {
      this.keyManager.setPreviousItemActive();
    } else if (code === 'ArrowRight') {
      this.keyManager.setNextItemActive();
    } else if (code === 'Tab' && !manager.activeItem) {
      this.keyManager.setFirstItemActive();
    }

    if (
      this.keyManager.activeItemIndex != null &&
      this.keyManager.activeItemIndex !== previousActiveIndex
    ) {
      this.index.set(this.keyManager.activeItemIndex);
      this.updateItemTabIndices();

      if (this.isOutOfView(this.index())) {
        this.scrollToActiveItem();
      }
    }
  }

  private updateItemTabIndices() {
    for (const item of this.items()) {
      if (this.keyManager != null) {
        item.tabindex = item === this.keyManager.activeItem ? '0' : '-1';
      }
    }
  }

  private scrollToActiveItem() {
    if (!this.isOutOfView(this.index())) {
      return;
    }

    const itemsArray = this.items();
    let targetItemIndex = this.index();

    if (this.index() > 0 && !this.isOutOfView(this.index() - 1)) {
      targetItemIndex =
        itemsArray.findIndex((_, i) => !this.isOutOfView(i)) + 1;
    }

    const newPosition =
      itemsArray[targetItemIndex].element.nativeElement.offsetLeft;
    this.list().nativeElement.style.transform = `translateX(-${newPosition}px)`;

    this.position.set(newPosition);

    this.hasPrevious.set(this.index() > 0);
    this.hasNext.set(false);

    for (let i = itemsArray.length - 1; i > -1; i--) {
      if (this.isOutOfView(i, 'end')) {
        this.hasNext.set(true);
        break;
      }
    }
  }

  private isOutOfView(index: number, side?: 'start' | 'end') {
    const { offsetWidth, offsetLeft } =
      this.items()[index].element.nativeElement;

    const position = this.position();

    if ((!side || side === 'start') && offsetLeft - position < 0) {
      return true;
    }

    return (
      (!side || side === 'end') &&
      offsetWidth + offsetLeft - position >
        this.list().nativeElement.clientWidth
    );
  }
}
