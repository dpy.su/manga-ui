import { ChangeDetectionStrategy, Component, input } from '@angular/core';
import { NavigateBackBtnComponent } from '@shared/ui';

@Component({
  selector: 'moe-page-header',
  standalone: true,
  imports: [NavigateBackBtnComponent],
  templateUrl: './page-header.component.html',
  styleUrl: './page-header.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PageHeaderComponent {
  public title = input.required<string>();
}
