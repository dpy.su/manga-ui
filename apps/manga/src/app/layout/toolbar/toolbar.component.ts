import {
  ChangeDetectionStrategy,
  Component,
  inject,
  input,
} from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { SidenavService } from '../sidenav/sidenav.service';
import { TranslocoModule } from '@jsverse/transloco';

@Component({
  selector: 'moe-toolbar',
  standalone: true,
  imports: [MatToolbarModule, MatButtonModule, MatIconModule, TranslocoModule],
  templateUrl: './toolbar.component.html',
  styleUrl: './toolbar.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolbarComponent {
  public sticky = input(false, { transform: coerceBooleanProperty });

  sidenav = inject(SidenavService);
}
