import {
  ChangeDetectionStrategy,
  Component,
  DestroyRef,
  inject,
  TemplateRef,
  viewChild,
  ViewContainerRef,
} from '@angular/core';
import { Overlay, ViewportRuler } from '@angular/cdk/overlay';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { TemplatePortal } from '@angular/cdk/portal';
import { filter, finalize, merge, Subject, take, takeUntil } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { SimpleSearchOverlayComponent } from '../simple-search-overlay/simple-search-overlay.component';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'moe-simple-search-bar',
  standalone: true,
  imports: [MatButtonModule, MatIconModule, SimpleSearchOverlayComponent],
  templateUrl: './simple-search-bar.component.html',
  styleUrl: './simple-search-bar.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SimpleSearchBarComponent {
  private _viewportRuler = inject(ViewportRuler);
  private _breakpointObserver = inject(BreakpointObserver);
  private _router = inject(Router);
  private _overlay = inject(Overlay);
  private _destroyRef = inject(DestroyRef);

  private _manualSearchClose$ = new Subject<void>();

  private _template = viewChild.required('overlayTpl', { read: TemplateRef });
  private _overlayTarget = viewChild.required('overlayTarget', {
    read: ViewContainerRef,
  });

  protected openSearchOverlay() {
    const portal = new TemplatePortal(this._template(), this._overlayTarget());

    const positionStrategy = this.getPositionStrategy();
    const width = this.getOverlayWidth();

    const overlayRef = this._overlay.create({
      positionStrategy,
      hasBackdrop: true,
      width,
    });
    overlayRef.attach(portal);

    const overlayDispose$ = merge(
      this._manualSearchClose$,
      this._router.events.pipe(filter((e) => e instanceof NavigationEnd)),
      overlayRef.outsidePointerEvents()
    );

    this._viewportRuler
      .change()
      .pipe(takeUntil(overlayDispose$))
      .subscribe(() => {
        const width = this.getOverlayWidth();
        overlayRef.updateSize({ width: width });

        const positionStrategy = this.getPositionStrategy();
        overlayRef.updatePositionStrategy(positionStrategy);
      });

    overlayDispose$
      .pipe(
        take(1),
        finalize(() => overlayRef.dispose()),
        takeUntilDestroyed(this._destroyRef)
      )
      .subscribe();
  }

  protected overlayClosed() {
    this._manualSearchClose$.next();
  }

  private getPositionStrategy() {
    const isMobile = this._breakpointObserver.isMatched(Breakpoints.XSmall);
    if (isMobile) {
      return this._overlay.position().global().top();
    }
    return this._overlay.position().global().centerHorizontally().top('1rem');
  }

  private getOverlayWidth() {
    if (this._breakpointObserver.isMatched(Breakpoints.XSmall)) {
      return '100%';
    }

    if (this._breakpointObserver.isMatched(Breakpoints.Small)) {
      return '75%';
    }

    if (this._breakpointObserver.isMatched(Breakpoints.Medium)) {
      return '65%';
    }

    if (
      this._breakpointObserver.isMatched([
        Breakpoints.Large,
        Breakpoints.XLarge,
      ])
    ) {
      return '55%';
    }

    throw new Error('Unreachable');
  }
}
