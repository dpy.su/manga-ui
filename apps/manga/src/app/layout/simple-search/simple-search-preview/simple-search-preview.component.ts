import { ChangeDetectionStrategy, Component, input } from '@angular/core';

import { SimpleSearchPreviewFragment } from '@generated-graphql';
import {
  MangaCoverComponent,
  MangaStatsComponent,
  MangaStatusComponent,
  MangaTitleComponent,
} from '@shared/manga';

@Component({
  selector: 'moe-simple-search-preview',
  standalone: true,
  imports: [
    MangaCoverComponent,
    MangaTitleComponent,
    MangaStatsComponent,
    MangaStatusComponent,
  ],
  templateUrl: './simple-search-preview.component.html',
  styleUrl: './simple-search-preview.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SimpleSearchPreviewComponent {
  public manga = input.required<SimpleSearchPreviewFragment>();
}
