import {
  ChangeDetectionStrategy,
  Component,
  inject,
  output,
} from '@angular/core';
import { AsyncPipe } from '@angular/common';
import { A11yModule } from '@angular/cdk/a11y';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { Repeat } from 'ngxtension/repeat';
import { SimpleSearchPreviewComponent } from '../simple-search-preview/simple-search-preview.component';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { derivedFrom } from 'ngxtension/derived-from';
import {
  combineLatest,
  debounceTime,
  filter,
  map,
  Observable,
  shareReplay,
  switchMap,
} from 'rxjs';
import {
  SimpleSearchGQL,
  SimpleSearchPreviewFragment,
} from '@generated-graphql';
import { OverlayModule } from '@angular/cdk/overlay';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { SkeletonComponent } from '@shared/ui';
import { TranslocoDirective } from '@jsverse/transloco';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'moe-simple-search-overlay',
  standalone: true,
  imports: [
    AsyncPipe,
    Repeat,
    OverlayModule,
    A11yModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatProgressBarModule,
    ReactiveFormsModule,
    SkeletonComponent,
    SimpleSearchPreviewComponent,
    TranslocoDirective,
  ],
  templateUrl: './simple-search-overlay.component.html',
  styleUrl: './simple-search-overlay.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SimpleSearchOverlayComponent {
  public overlayClose = output();

  protected searchControl = new FormControl('', { nonNullable: true });
  protected searchTerm$ = this.searchControl.valueChanges.pipe(
    debounceTime(300)
  );

  private _searchGQL = inject(SimpleSearchGQL);
  private _queryResult = this.searchTerm$.pipe(
    filter(x => x.length !== 0),
    switchMap(term => this._searchGQL.watch({ term }).valueChanges),
    takeUntilDestroyed(),
    shareReplay(1)
  );

  protected loading$ = this._queryResult.pipe(map(x => x.loading));

  protected manga$: Observable<SimpleSearchPreviewFragment[]> =
    this._queryResult.pipe(
      filter(x => !x.loading),
      map(x => x.data.mangas.items)
    );

  protected showManga$ = combineLatest([this.searchTerm$, this.loading$]).pipe(
    map(([term, loading]) => term && !loading)
  );

  private _breakpointObserver = inject(BreakpointObserver);
  protected isMobile = derivedFrom(
    [this._breakpointObserver.observe(Breakpoints.XSmall)],
    map(([state]) => state.matches),
    { initialValue: false }
  );

  protected closeOverlay() {
    this.overlayClose.emit();
  }
}
