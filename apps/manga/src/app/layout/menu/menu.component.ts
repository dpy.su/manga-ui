import {
  ChangeDetectionStrategy,
  Component,
  inject,
  signal,
} from '@angular/core';
import { NavigationStart, Router, RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { OverlayModule } from '@angular/cdk/overlay';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { ThemeToggleComponent } from '../theme-toggle/theme-toggle.component';
import { MatLineModule } from '@angular/material/core';
import { filter } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MatCardModule } from '@angular/material/card';
import { UserProfileComponent } from '../user-profile/user-profile.component';
import { AuthService, UserService } from '@core/auth';
import { TranslocoDirective } from '@jsverse/transloco';

interface MenuItem {
  icon: string;
  translateKey: string;
  path: string;
}

@Component({
  selector: 'moe-menu',
  templateUrl: './menu.component.html',
  styleUrl: './menu.component.scss',
  standalone: true,
  imports: [
    RouterModule,
    MatButtonModule,
    MatIconModule,
    MatLineModule,
    MatListModule,
    MatCardModule,
    OverlayModule,
    ThemeToggleComponent,
    UserProfileComponent,
    TranslocoDirective,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuComponent {
  public readonly menuItems: readonly MenuItem[] = [
    { icon: 'person', translateKey: 'PROFILE', path: 'profile' },
    { icon: 'bookmark', translateKey: 'BOOKMARKS', path: 'bookmarks' },
    { icon: 'group', translateKey: 'GROUPS', path: 'my-groups' },
  ];

  public menuOpen = signal(false);

  public readonly authService = inject(AuthService);
  public readonly userService = inject(UserService);
  private readonly router = inject(Router);

  constructor() {
    this.router.events
      .pipe(
        filter((e) => e instanceof NavigationStart && this.menuOpen()),
        takeUntilDestroyed()
      )
      .subscribe(() => this.menuOpen.set(false));
  }
}
