import {
  ChangeDetectionStrategy,
  Component,
  computed,
  inject,
  Signal,
} from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ThemeService } from '@core/theming';

type ThemeIcon = 'light_mode' | 'dark_mode';
type AnimationState = 'light' | 'dark';

@Component({
  selector: 'moe-theme-toggle',
  standalone: true,
  imports: [MatButtonModule, MatIconModule],
  templateUrl: './theme-toggle.component.html',
  styleUrl: './theme-toggle.component.scss',
  animations: [
    trigger('themeSwitch', [
      state(
        'light',
        style({
          transform: 'rotate(-180deg)',
        })
      ),
      state(
        'dark',
        style({
          transform: 'rotate(0deg)',
        })
      ),
      transition('light <=> dark', animate('500ms ease-in-out')),
    ]),
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ThemeToggleComponent {
  protected icon: Signal<ThemeIcon>;
  protected animationState: Signal<AnimationState>;
  private readonly themeService = inject(ThemeService);

  constructor() {
    this.icon = computed(() => {
      return this.themeService.theme() === 'light' ? 'light_mode' : 'dark_mode';
    });
    this.animationState = this.themeService.theme;
  }

  protected toggleTheme(): void {
    this.themeService.toggle();
  }
}
