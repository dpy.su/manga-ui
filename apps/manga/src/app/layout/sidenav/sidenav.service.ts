import { Injectable, signal } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SidenavService {
  private readonly _opened = signal(false);
  public readonly opened = this._opened.asReadonly();

  public close(): void {
    this._opened.set(false);
  }

  public open(): void {
    this._opened.set(true);
  }

  public toggle(): void {
    this._opened.update((v) => !v);
  }
}
