import {
  ChangeDetectionStrategy,
  Component,
  inject,
  Signal,
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { RouterModule } from '@angular/router';
import { MatLineModule } from '@angular/material/core';
import { TranslocoModule } from '@jsverse/transloco';
import { MatDrawerMode, MatSidenavModule } from '@angular/material/sidenav';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map } from 'rxjs';
import { derivedFrom } from 'ngxtension/derived-from';
import { SidenavService } from './sidenav.service';

interface SidenavLink {
  icon: string;
  translateKey: string;
  path: string;
  exactMatch?: boolean;
}

@Component({
  selector: 'moe-sidenav',
  standalone: true,
  imports: [
    RouterModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatLineModule,
    MatSidenavModule,
    TranslocoModule,
  ],
  templateUrl: './sidenav.component.html',
  styleUrl: './sidenav.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SidenavComponent {
  protected readonly links: SidenavLink[] = [
    { path: '/', icon: 'home', translateKey: 'HOME', exactMatch: true },
    {
      path: '/advanced-search',
      icon: 'search',
      translateKey: 'ADVANCED_SEARCH',
    },
  ];

  protected readonly sidenav = inject(SidenavService);

  private readonly breakpointObserver = inject(BreakpointObserver);
  protected readonly sidenavMode: Signal<MatDrawerMode> = derivedFrom(
    [this.breakpointObserver.observe([Breakpoints.XSmall, Breakpoints.Small])],
    map(([state]) => (state.matches ? 'over' : 'side')),
    { initialValue: 'side' }
  );
}
