import { ChangeDetectionStrategy, Component, input } from '@angular/core';
import { AuthUser } from '@core/auth';
import { NgOptimizedImage } from '@angular/common';

@Component({
  selector: 'moe-user-profile',
  standalone: true,
  imports: [NgOptimizedImage],
  templateUrl: './user-profile.component.html',
  styleUrl: './user-profile.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserProfileComponent {
  public user = input.required<AuthUser>();
}
