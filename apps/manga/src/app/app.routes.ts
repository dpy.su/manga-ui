import { Routes } from '@angular/router';
import { requireAuthGuard } from '@core/auth';

export const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/home/routes'),
  },
  {
    path: 'settings',
    loadChildren: () => import('./pages/settings/routes'),
  },
  {
    path: 'bookmarks',
    loadChildren: () => import('./pages/bookmarks/routes'),
    canActivate: [requireAuthGuard()],
  },
  {
    path: 'advanced-search',
    loadChildren: () => import('./pages/advanced-search/routes'),
  },
  {
    path: 'manga',
    loadChildren: () => import('./pages/manga/routes'),
  },
  {
    path: 'auth/sign-in-callback',
    loadChildren: () => import('./pages/auth/routes'),
  },
  { path: '**', redirectTo: '' },
];
