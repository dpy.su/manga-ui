import {
  ApplicationConfig,
  inject,
  provideAppInitializer,
  provideZoneChangeDetection,
} from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import {
  DomSanitizer,
  provideClientHydration,
  withEventReplay,
  withHttpTransferCacheOptions,
} from '@angular/platform-browser';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import {
  provideHttpClient,
  withFetch,
  withInterceptors,
} from '@angular/common/http';
import { authInterceptor } from 'angular-auth-oidc-client';
import { provideTheming } from '@core/theming';
import { provideOIDCAuth } from '@core/auth';
import { MatIconRegistry } from '@angular/material/icon';
import { BIND_QUERY_PARAMS_OPTIONS } from '@ngneat/bind-query-params';
import { WINDOW } from '@core/injection-tokens';
import { provideLocalization } from '@core/localization';
import { provideGraphql } from '@core/graphql.provider';

const initializeApp = () => {
  const sanitizer = inject(DomSanitizer);
  const iconRegistry = inject(MatIconRegistry);

  iconRegistry.addSvgIconSetInNamespace(
    'flags',
    sanitizer.bypassSecurityTrustResourceUrl('./assets/icons/flags.svg')
  );
};

export const appConfig: ApplicationConfig = {
  providers: [
    provideZoneChangeDetection({ eventCoalescing: true }),
    provideRouter(routes),
    provideClientHydration(
      withHttpTransferCacheOptions({ includePostRequests: true }),
      withEventReplay()
    ),
    provideAnimationsAsync(),
    provideHttpClient(withFetch(), withInterceptors([authInterceptor()])),

    // Custom providers
    provideGraphql(),
    provideTheming(),
    provideOIDCAuth(),
    provideLocalization(),

    provideAppInitializer(initializeApp),
    {
      provide: BIND_QUERY_PARAMS_OPTIONS,
      useFactory: () => {
        return { windowRef: inject(WINDOW) };
      },
    },
  ],
};
