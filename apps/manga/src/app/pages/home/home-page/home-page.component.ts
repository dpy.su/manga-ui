import { ChangeDetectionStrategy, Component } from '@angular/core';
import { LatestUpdatesSectionComponent } from '../latest-updates-section/latest-updates-section.component';

@Component({
  selector: 'moe-home-page',
  standalone: true,
  imports: [LatestUpdatesSectionComponent],
  templateUrl: './home-page.component.html',
  styleUrl: './home-page.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomePageComponent {}
