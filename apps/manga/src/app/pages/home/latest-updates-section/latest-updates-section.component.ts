import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { LatestUpdatesSectionGQL, MangaTileFragment } from '@generated-graphql';
import { filter, map, share } from 'rxjs';
import { toSignal } from '@angular/core/rxjs-interop';
import { CarouselComponent, CarouselItemDirective } from '@shared/ui';
import { MangaTileComponent } from '@shared/manga';
import { SectionHeaderComponent } from '../section-header/section-header.component';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { TranslocoDirective } from '@jsverse/transloco';

@Component({
  selector: 'moe-latest-updates-section',
  standalone: true,
  imports: [
    CarouselComponent,
    MangaTileComponent,
    CarouselItemDirective,
    SectionHeaderComponent,
    MatProgressSpinner,
    TranslocoDirective,
  ],
  templateUrl: './latest-updates-section.component.html',
  styleUrl: './latest-updates-section.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LatestUpdatesSectionComponent {
  private latestUpdatesSectionGQL = inject(LatestUpdatesSectionGQL);

  private queryResult = this.latestUpdatesSectionGQL
    .watch()
    .valueChanges.pipe(share());

  protected showLoader = toSignal<boolean>(
    this.queryResult.pipe(map((x) => x.loading))
  );
  protected manga = toSignal<MangaTileFragment[]>(
    this.queryResult.pipe(
      filter((x) => !x.loading),
      map((x) => x.data.mangas.items)
    )
  );
}
