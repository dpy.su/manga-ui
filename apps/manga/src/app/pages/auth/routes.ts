import { Routes } from '@angular/router';
import { PostSignInPageComponent } from './post-sign-in-page/post-sign-in-page.component';

export default [
  {
    path: '',
    component: PostSignInPageComponent,
    title: 'PAGES.BOOKMARKS.TITLE',
  },
] satisfies Routes;
