import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { TranslocoDirective } from '@jsverse/transloco';

@Component({
  selector: 'moe-post-sign-in-page',
  standalone: true,
  imports: [MatProgressSpinner, TranslocoDirective],
  templateUrl: './post-sign-in-page.component.html',
  styleUrl: './post-sign-in-page.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PostSignInPageComponent {
  // private readonly authService = inject(OidcSecurityService);
}
