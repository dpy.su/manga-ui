import {
  ChangeDetectionStrategy,
  Component,
  inject,
  input,
  signal,
} from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  MangaActionButtonComponent,
  MangaActionButtonContent,
  MangaActionButtonOverlayContent,
} from '../manga-action-button/manga-action-button.component';
import { SetMangaRatingGQL, UserRatingFragment } from '@generated-graphql';
import { MatCard } from '@angular/material/card';
import { MatListOption, MatSelectionList } from '@angular/material/list';
import { KeyValuePipe } from '@angular/common';
import { MatButton } from '@angular/material/button';
import { filter, tap } from 'rxjs';

const MANGA_RATINGS = new Map<number, string>([
  [1, 'APPALLING'],
  [2, 'HORRIBLE'],
  [3, 'VERY_BAD'],
  [4, 'BAD'],
  [5, 'AVERAGE'],
  [6, 'FINE'],
  [7, 'GOOD'],
  [8, 'VERY_GOOD'],
  [9, 'GREAT'],
  [10, 'MASTERPIECE'],
]);

@Component({
  selector: 'moe-manga-rate-button',
  imports: [
    TranslocoDirective,
    MangaActionButtonComponent,
    MangaActionButtonOverlayContent,
    MangaActionButtonContent,
    MatCard,
    MatSelectionList,
    MatListOption,
    KeyValuePipe,
    MatButton,
  ],
  templateUrl: './manga-rate-button.component.html',
  styleUrl: './manga-rate-button.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MangaRateButtonComponent {
  private setRatingGQL = inject(SetMangaRatingGQL);

  protected mangaRatings = MANGA_RATINGS;

  public manga = input.required<UserRatingFragment>();

  protected ratingLoading = signal(false);

  protected handleStatusChange(rating: number | null) {
    if (rating == null) {
      this.setRating(null);
    } else {
      this.setRating(rating);
    }
  }

  private setRating(ratingValue: number | null): void {
    this.setRatingGQL
      .mutate(
        { input: { rating: ratingValue, mangaId: this.manga().id } },
        { refetchQueries: 'active' }
      )
      .pipe(
        tap(x => this.ratingLoading.set(x?.loading ?? false)),
        filter(x => !x.loading)
      )
      .subscribe();
  }
}
