import { Component, inject, input, linkedSignal } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MangaChapterItemFragment,
  MangaChaptersGQL,
  PagePaginationInfo,
} from '@generated-graphql';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import { combineLatest, filter, map, share, switchMap } from 'rxjs';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { groupBy, OrderByPipe, Pagination } from '@core/helpers';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { VolumeGroupComponent } from '../volume-group/volume-group.component';

type ChaptersByVolume = ReadonlyMap<number | null, MangaChapterItemFragment[]>;

@Component({
  selector: 'moe-manga-chapters',
  imports: [
    CommonModule,
    MatPaginator,
    MatProgressSpinner,
    OrderByPipe,
    VolumeGroupComponent,
  ],
  templateUrl: './manga-chapters.component.html',
  styleUrl: './manga-chapters.component.scss',
})
export class MangaChaptersComponent {
  protected readonly PAGE_SIZE = 30;

  public mangaId = input.required<string>();

  private _mangaChaptersGQL = inject(MangaChaptersGQL);

  private pagination = linkedSignal<string, Pagination>({
    source: () => this.mangaId(),
    computation: () => ({ pageSize: this.PAGE_SIZE, page: 1 }),
  });

  private _queryResult = combineLatest([
    toObservable(this.mangaId),
    toObservable(this.pagination),
  ]).pipe(
    switchMap(
      ([mangaId, pagination]) =>
        this._mangaChaptersGQL.watch({ mangaId, pagination }).valueChanges
    ),
    share()
  );

  protected showLoader = toSignal(this._queryResult.pipe(map(x => x.loading)));

  protected chaptersByVolume = toSignal<ChaptersByVolume | null>(
    this._queryResult.pipe(
      filter(x => !x.loading || x.data != null),
      map(x => x.data.manga?.chapters.items ?? []),
      map(x => groupBy(x, c => c.volume ?? null))
    ),
    { initialValue: null }
  );
  protected pageInfo = toSignal<PagePaginationInfo | null>(
    this._queryResult.pipe(
      filter(x => !x.loading || x.data != null),
      map(x => x.data.manga?.chapters.pageInfo ?? null)
    ),
    { initialValue: null }
  );

  protected onPageChange(event: PageEvent) {
    this.pagination.set({
      pageSize: event.pageSize,
      page: event.pageIndex + 1,
    });
  }
}
