import { Component, computed, effect, input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MangaChapterItemFragment } from '@generated-graphql';
import { MatTableModule } from '@angular/material/table';
import { MatIconButton } from '@angular/material/button';
import { MatIcon } from '@angular/material/icon';
import { SelectionModel } from '@angular/cdk/collections';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { groupBy, OrderByPipe } from '@core/helpers';
import { TranslocoDirective } from '@jsverse/transloco';
import { CdkTableModule } from '@angular/cdk/table';
import { ChapterListItemComponent } from '../chapter-list-item/chapter-list-item.component';
import { MatActionList, MatListItem } from '@angular/material/list';
import { MatDivider } from '@angular/material/divider';

@Component({
  selector: 'moe-chapter-list',
  imports: [
    CommonModule,
    MatIconButton,
    MatIcon,
    CdkTableModule,
    MatTableModule,
    OrderByPipe,
    TranslocoDirective,
    ChapterListItemComponent,
    MatListItem,
    MatDivider,
    MatActionList,
  ],
  templateUrl: './chapter-list.component.html',
  styleUrl: './chapter-list.component.scss',
  animations: [
    trigger('contentExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class ChapterListComponent {
  protected readonly MAIN_COLUMN = 'main';
  protected readonly EXPANDER_COLUMN = 'expander';
  protected readonly EXPANDED_CONTENT_COLUMN = 'expandedContent';

  protected readonly DISPLAYED_COLUMNS = [
    this.MAIN_COLUMN,
    this.EXPANDER_COLUMN,
  ];

  public chapters = input.required<MangaChapterItemFragment[]>();

  protected chaptersByNumber = computed(() =>
    groupBy(this.chapters(), x => x.number)
  );

  protected expandedChapterNumbers = new SelectionModel<string>(true, [], true);

  constructor() {
    effect(() => {
      const chapters = this.chapters();
      this.expandedChapterNumbers.select(...chapters.map(x => x.number));
    });
  }
}
