import { Component, computed, inject, input, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MangaDescriptionFragment } from '@generated-graphql';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { MatButton } from '@angular/material/button';
import { MatIcon } from '@angular/material/icon';
import { derivedFrom } from 'ngxtension/derived-from';
import { map } from 'rxjs';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { TranslocoDirective } from '@jsverse/transloco';

type ExpandButtonParams =
  | {
      icon: 'unfold_less_double';
      text: 'COMPONENTS.MANGA_DESCRIPTION.SHOW_LESS';
    }
  | {
      icon: 'unfold_more_double';
      text: 'COMPONENTS.MANGA_DESCRIPTION.SHOW_MORE';
    };

@Component({
  selector: 'moe-manga-description',
  imports: [CommonModule, MatButton, MatIcon, TranslocoDirective],
  templateUrl: './manga-description.component.html',
  styleUrl: './manga-description.component.scss',
  animations: [
    trigger('expandDescription', [
      state(
        'collapsed',
        style({
          height: '75px',
          overflow: 'hidden',
        })
      ),
      state('expanded', style({ height: '*', overflow: 'hidden' })),
      transition('collapsed <=> expanded', animate('350ms ease-in-out')),
    ]),
  ],
})
export class MangaDescriptionComponent {
  private readonly _breakpointObserver = inject(BreakpointObserver);

  private _descriptionExpanded = signal(false);
  protected descriptionExpanded = this._descriptionExpanded.asReadonly();

  public manga = input.required<MangaDescriptionFragment>();

  protected isSmallScreen = derivedFrom(
    [this._breakpointObserver.observe([Breakpoints.XSmall, Breakpoints.Small])],
    map(([state]) => state.matches)
  );

  protected animationState = computed(() => {
    if (this.isSmallScreen()) {
      return this.descriptionExpanded() ? 'expanded' : 'collapsed';
    }

    return 'expanded';
  });

  protected expandButtonParams = computed<ExpandButtonParams>(() =>
    this.descriptionExpanded()
      ? {
          icon: 'unfold_less_double',
          text: 'COMPONENTS.MANGA_DESCRIPTION.SHOW_LESS',
        }
      : {
          icon: 'unfold_more_double',
          text: 'COMPONENTS.MANGA_DESCRIPTION.SHOW_MORE',
        }
  );

  protected toggleDescription() {
    this._descriptionExpanded.update(x => !x);
  }
}
