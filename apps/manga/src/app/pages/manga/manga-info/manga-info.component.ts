import {
  ChangeDetectionStrategy,
  Component,
  inject,
  input,
} from '@angular/core';
import { filter, map, share, switchMap } from 'rxjs';
import { MangaInfoFragment, MangaInfoGQL } from '@generated-graphql';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import {
  MangaCoverComponent,
  MangaStatsComponent,
  MangaStatusComponent,
  MangaTagsComponent,
} from '@shared/manga';
import { MangaBookmarkButtonComponent } from '../manga-bookmark-button/manga-bookmark-button.component';
import { MangaRateButtonComponent } from '../manga-rate-button/manga-rate-button.component';
import { MangaReadButtonComponent } from '../manga-read-button/manga-read-button.component';
import { MangaDescriptionComponent } from '../manga-description/manga-description.component';
import { MangaChaptersComponent } from '../manga-chapters/manga-chapters.component';

@Component({
  selector: 'moe-manga-info',
  standalone: true,
  imports: [
    MangaCoverComponent,
    MangaStatsComponent,
    MangaTagsComponent,
    MangaBookmarkButtonComponent,
    MangaRateButtonComponent,
    MangaReadButtonComponent,
    MangaStatusComponent,
    MangaDescriptionComponent,
    MangaChaptersComponent,
  ],
  templateUrl: './manga-info.component.html',
  styleUrl: './manga-info.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MangaInfoComponent {
  public mangaId = input.required<string>();

  private _mangaInfoGQL = inject(MangaInfoGQL);
  private _queryResult = toObservable(this.mangaId).pipe(
    switchMap(mangaId => {
      return this._mangaInfoGQL.watch({ mangaId }, { errorPolicy: 'all' })
        .valueChanges;
    }),
    share()
  );

  protected showLoader = toSignal<boolean>(
    this._queryResult.pipe(map(x => x.loading))
  );

  protected manga = toSignal<MangaInfoFragment | null>(
    this._queryResult.pipe(
      filter(x => !x.loading || x.data != null),
      map(x => x.data.manga ?? null)
    ),
    { initialValue: null }
  );
}
