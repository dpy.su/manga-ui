import {
  ChangeDetectionStrategy,
  Component,
  contentChild,
  Directive,
  inject,
  input,
  signal,
  TemplateRef,
} from '@angular/core';
import { MatFabButton } from '@angular/material/button';
import { MatIcon } from '@angular/material/icon';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { AuthService, UserService } from '@core/auth';
import { CdkConnectedOverlay, CdkOverlayOrigin } from '@angular/cdk/overlay';
import { NgTemplateOutlet } from '@angular/common';

/* eslint-disable-next-line @angular-eslint/directive-selector */
@Directive({ selector: 'ng-template[actionButtonContent]' })
/* eslint-disable-next-line @angular-eslint/directive-class-suffix */
export class MangaActionButtonContent {}

/* eslint-disable-next-line @angular-eslint/directive-selector */
@Directive({ selector: 'ng-template[actionButtonOverlayContent]' })
/* eslint-disable-next-line @angular-eslint/directive-class-suffix */
export class MangaActionButtonOverlayContent {}

@Component({
  selector: 'moe-manga-action-button',
  imports: [
    MatIcon,
    MatProgressSpinner,
    CdkOverlayOrigin,
    CdkConnectedOverlay,
    NgTemplateOutlet,
    MatFabButton,
  ],
  templateUrl: './manga-action-button.component.html',
  styleUrl: './manga-action-button.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MangaActionButtonComponent {
  private _userService = inject(UserService);
  private _authService = inject(AuthService);

  public icon = input.required<string>();
  public loading = input(false);

  protected buttonContent = contentChild(MangaActionButtonContent, {
    read: TemplateRef,
  });

  protected overlayContent = contentChild.required(
    MangaActionButtonOverlayContent,
    {
      read: TemplateRef,
    }
  );

  protected overlayOpen = signal(false);

  public toggleOverlay() {
    this.overlayOpen.update(x => !x);
  }

  protected handleClick(): void {
    if (this._userService.isAnonymous()) {
      this._authService.login();
      return;
    }

    this.toggleOverlay();
  }
}
