import {
  ChangeDetectionStrategy,
  Component,
  inject,
  input,
  signal,
} from '@angular/core';
import {
  AddBookmarkGQL,
  MangaBookmarkStatus,
  RemoveBookmarkGQL,
  UserBookmarkFragment,
} from '@generated-graphql';
import {
  MangaActionButtonComponent,
  MangaActionButtonOverlayContent,
} from '../manga-action-button/manga-action-button.component';
import { filter, tap } from 'rxjs';
import { MatListOption, MatSelectionList } from '@angular/material/list';
import { MatButton } from '@angular/material/button';
import { MatCard } from '@angular/material/card';
import { TranslocoDirective } from '@jsverse/transloco';

const BOOKMARK_STATUSES = Object.values<MangaBookmarkStatus>(
  MangaBookmarkStatus
)
  .sort()
  .reverse();

@Component({
  selector: 'moe-manga-bookmark-button',
  imports: [
    MangaActionButtonComponent,
    MatSelectionList,
    MatListOption,
    MatButton,
    MatCard,
    TranslocoDirective,
    MangaActionButtonOverlayContent,
  ],
  templateUrl: './manga-bookmark-button.component.html',
  styleUrl: './manga-bookmark-button.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MangaBookmarkButtonComponent {
  protected readonly bookmarkStatuses = BOOKMARK_STATUSES;

  private addBookmarkGQL = inject(AddBookmarkGQL);
  private removeBookmarkGQL = inject(RemoveBookmarkGQL);

  public manga = input.required<UserBookmarkFragment>();

  protected bookmarkLoading = signal(false);

  protected handleStatusChange(status: MangaBookmarkStatus | null) {
    if (status == null) {
      this.removeBookmark();
    } else {
      this.addBookmark(status);
    }
  }

  private addBookmark(status: MangaBookmarkStatus): void {
    this.addBookmarkGQL
      .mutate(
        { input: { mangaId: this.manga().id, status } },
        { refetchQueries: 'active' }
      )
      .pipe(
        tap(x => this.bookmarkLoading.set(x?.loading ?? false)),
        filter(x => !x.loading)
      )
      .subscribe();
  }

  private removeBookmark(): void {
    this.removeBookmarkGQL
      .mutate({ mangaId: this.manga().id }, { refetchQueries: 'active' })
      .pipe(
        tap(x => this.bookmarkLoading.set(x?.loading ?? false)),
        filter(x => !x.loading)
      )
      .subscribe();
  }
}
