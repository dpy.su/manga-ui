import { Component, computed, input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  MatExpansionPanel,
  MatExpansionPanelDescription,
  MatExpansionPanelHeader,
  MatExpansionPanelTitle,
} from '@angular/material/expansion';
import { MangaChapterItemFragment } from '@generated-graphql';
import { ChapterListComponent } from '../chapter-list/chapter-list.component';

type ChapterRange = { first: string; last?: string };

@Component({
  selector: 'moe-volume-group',
  imports: [
    CommonModule,
    TranslocoDirective,
    MatExpansionPanel,
    MatExpansionPanelTitle,
    MatExpansionPanelHeader,
    MatExpansionPanelDescription,
    ChapterListComponent,
  ],
  templateUrl: './volume-group.component.html',
  styleUrl: './volume-group.component.scss',
})
export class VolumeGroupComponent {
  public volumeNumber = input.required<number | null>();
  public chapters = input.required<MangaChapterItemFragment[]>();

  protected chapterRange = computed<ChapterRange>(() => {
    const chapters = this.chapters();
    if (chapters.length <= 1) {
      return { first: chapters[0].number };
    }

    return {
      first: chapters[0].number,
      last: chapters[chapters.length - 1].number,
    };
  });
}
