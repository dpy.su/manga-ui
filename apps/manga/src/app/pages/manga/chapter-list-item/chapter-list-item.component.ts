import { Component, input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LanguageEnum, MangaChapterItemFragment } from '@generated-graphql';
import { MatIcon } from '@angular/material/icon';
import { LanguageIconComponent } from '@shared/ui';
import { TranslocoDirective } from '@jsverse/transloco';

@Component({
  selector: 'moe-chapter-list-item',
  imports: [
    CommonModule,
    MatIcon,
    LanguageIconComponent,
    TranslocoDirective,
  ],
  templateUrl: './chapter-list-item.component.html',
  styleUrl: './chapter-list-item.component.scss',
})
export class ChapterListItemComponent {
  protected LANGUAGE = LanguageEnum.Ukr;

  public chapter = input.required<MangaChapterItemFragment>();
}
