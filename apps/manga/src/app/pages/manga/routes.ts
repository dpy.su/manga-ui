import { Routes } from '@angular/router';
import { MangaPageComponent } from './manga-page/manga-page.component';

export default [
  {
    path: ':mangaId',
    component: MangaPageComponent
  },
  {
    path: ':mangaId/:slug',
    component: MangaPageComponent,
  },
] satisfies Routes;
