import { ChangeDetectionStrategy, Component } from '@angular/core';
import { injectParams } from 'ngxtension/inject-params';
import { MangaInfoComponent } from '../manga-info/manga-info.component';

@Component({
  selector: 'moe-manga-page',
  standalone: true,
  imports: [MangaInfoComponent],
  templateUrl: './manga-page.component.html',
  styleUrl: './manga-page.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MangaPageComponent {
  protected mangaId = injectParams('mangaId');
}
