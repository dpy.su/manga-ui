import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatFabButton } from '@angular/material/button';
import { TranslocoDirective } from '@jsverse/transloco';
import { MatIcon } from '@angular/material/icon';

@Component({
  selector: 'moe-manga-read-button',
  imports: [CommonModule, MatFabButton, MatIcon, TranslocoDirective],
  templateUrl: './manga-read-button.component.html',
  styleUrl: './manga-read-button.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MangaReadButtonComponent {}
