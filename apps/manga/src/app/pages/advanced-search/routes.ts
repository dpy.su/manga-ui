import { Routes } from '@angular/router';
import { AdvancedSearchService } from './advanced-search-page/advanced-search.service';
import { AdvancedSearchPageComponent } from './advanced-search-page/advanced-search-page.component';

export default [
  {
    path: '',
    component: AdvancedSearchPageComponent,
    providers: [AdvancedSearchService],
    title: 'PAGES.ADVANCED_SEARCH.TITLE',
  },
] satisfies Routes;
