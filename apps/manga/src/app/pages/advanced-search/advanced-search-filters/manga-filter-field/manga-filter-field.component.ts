import { ChangeDetectionStrategy, Component, input } from '@angular/core';

@Component({
  selector: 'moe-manga-filter-field',
  standalone: true,
  imports: [],
  templateUrl: './manga-filter-field.component.html',
  styleUrl: './manga-filter-field.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MangaFilterFieldComponent {
  public label = input('');
}
