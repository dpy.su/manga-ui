import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MangaSortField, SortDirection } from '@generated-graphql';
import { MatFormField } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import {
  NgxControlValueAccessor,
  NgxControlValueAccessorCompareTo,
  provideCvaCompareTo,
} from 'ngxtension/control-value-accessor';
import { TranslocoDirective } from '@jsverse/transloco';

export interface SortValue {
  field: MangaSortField;
  direction: SortDirection;
}

const sortValueComparator: NgxControlValueAccessorCompareTo<SortValue> = (
  a,
  b
) => a?.field === b?.field && a?.direction === b?.direction;

@Component({
  selector: 'moe-manga-sort-filter',
  standalone: true,
  imports: [MatFormField, MatSelectModule, TranslocoDirective],
  hostDirectives: [NgxControlValueAccessor],
  providers: [provideCvaCompareTo(sortValueComparator, true)],
  templateUrl: './manga-sort-filter.component.html',
  styleUrl: './manga-sort-filter.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MangaSortFilterComponent {
  protected readonly sortValues = this.createSortValues();

  protected readonly cva = inject<NgxControlValueAccessor<SortValue | null>>(
    NgxControlValueAccessor
  );
  protected readonly sortValueComparator = sortValueComparator;

  private createSortValues(): SortValue[] {
    const result: SortValue[] = [];

    for (const fieldKey in MangaSortField) {
      for (const directionKey in SortDirection) {
        result.push({
          field: MangaSortField[fieldKey as keyof typeof MangaSortField],
          direction: SortDirection[directionKey as keyof typeof SortDirection],
        });
      }
    }

    return result;
  }
}
