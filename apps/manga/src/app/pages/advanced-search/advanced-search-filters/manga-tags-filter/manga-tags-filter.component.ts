import {
  ChangeDetectionStrategy,
  Component,
  computed,
  inject,
  untracked,
} from '@angular/core';
import {
  MangaTagChipComponent,
  SelectedMangaTag,
} from '../manga-tag-chip/manga-tag-chip.component';
import { MangaTag, MangaTagsGQL } from '@generated-graphql';
import {
  FormControl,
  NonNullableFormBuilder,
  ReactiveFormsModule,
} from '@angular/forms';
import { AsyncPipe, KeyValuePipe } from '@angular/common';
import {
  combineLatest,
  filter,
  map,
  Observable,
  startWith,
  switchMap
} from 'rxjs';
import {
  takeUntilDestroyed,
  toObservable,
  toSignal,
} from '@angular/core/rxjs-interop';
import {
  NgxControlValueAccessor,
  NgxControlValueAccessorCompareTo,
  provideCvaCompareTo,
} from 'ngxtension/control-value-accessor';
import { MatCardModule } from '@angular/material/card';
import {
  CustomSelectInputControl,
  SearchInputComponent,
  SkeletonComponent,
} from '@shared/ui';
import { Repeat } from 'ngxtension/repeat';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';
import { groupBy, isEqual } from 'lodash-es';
import { notNil } from '@core/helpers';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';

export interface SelectedTags {
  exclude: string[];
  include: string[];
}

const searchedTagsComparator: NgxControlValueAccessorCompareTo<SelectedTags> = (
  a,
  b
) => {
  if (!a && !b) {
    return true;
  }

  if (!a || !b) {
    return false;
  }

  if (!isEqual(a.include, b.include)) {
    return false;
  }

  return isEqual(a.exclude, b.exclude);
};

type MangaTagsGroup = { [tagGroupName: string]: MangaTag[] };

type TagsMap = ReadonlyMap<string, MangaTag>;

@Component({
  selector: 'moe-manga-tags-filter',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    KeyValuePipe,
    AsyncPipe,
    Repeat,
    MatCardModule,
    MatButtonModule,
    MatDividerModule,
    MangaTagChipComponent,
    SkeletonComponent,
    SearchInputComponent,
    TranslocoDirective,
  ],
  hostDirectives: [NgxControlValueAccessor],
  providers: [
    {
      provide: CustomSelectInputControl,
      useExisting: MangaTagsFilterComponent,
    },
    provideCvaCompareTo(searchedTagsComparator, true),
  ],
  templateUrl: './manga-tags-filter.component.html',
  styleUrl: './manga-tags-filter.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MangaTagsFilterComponent
  implements CustomSelectInputControl<SelectedTags | null>
{
  public readonly cva = inject<NgxControlValueAccessor<SelectedTags | null>>(
    NgxControlValueAccessor
  );

  protected tagGroupsControl = computed(() => this.createTagGroupsControl());

  private _fb = inject(NonNullableFormBuilder);
  private _translateService = inject(TranslocoService);

  protected filterControl = this._fb.control('');

  private _mangaTagsGQL = inject(MangaTagsGQL);
  private tags$: Observable<MangaTag[]> = this._mangaTagsGQL
    .watch()
    .valueChanges.pipe(
      filter((x) => !x.loading),
      map((x) => x.data.tags)
    );

  private _tags = toSignal(this.tags$, { initialValue: [] });
  private _tagsMap = computed<TagsMap>(() => this.createTagsMap());

  public readonly triggerValue = combineLatest([
    toObservable(this.cva.value$),
    toObservable(this._tagsMap),
    this._translateService.selectTranslateObject('COMPONENTS.SELECT_TAGS_LIST'),
  ]).pipe(
    map(([tags, tagsMap, keys]) => this.createTriggerValue(tags, tagsMap, keys))
  );
  private tagGroups$: Observable<MangaTagsGroup> = this.tags$.pipe(
    map((tags) => groupBy(tags, (k) => k.category.name))
  );
  protected filteredTagGroups$ = combineLatest([
    this.filterControl.valueChanges.pipe(startWith('')),
    this.tagGroups$,
  ]).pipe(
    map(([searchedTag, tagGroups]) =>
      this.filterTagGroups(searchedTag, tagGroups)
    )
  );

  constructor() {
    toObservable(this.tagGroupsControl)
      .pipe(
        switchMap((control) => control.valueChanges),
        map((value) => Object.values(value)),
        takeUntilDestroyed()
      )
      .subscribe((tags) => this.notifySelectionChanges(tags));

    this.cva.valueChange
      .pipe(
        filter((val) => val === null),
        takeUntilDestroyed()
      )
      .subscribe(() => this.resetControlValue());
  }

  public get isEmpty(): boolean {
    const val = this.cva.value;
    if (!val) {
      return true;
    }
    return val?.include.length == 0 && val?.exclude.length == 0;
  }

  public resetControlValue() {
    this.tagGroupsControl().reset();
  }

  private createTagsMap() {
    const tags = this._tags();

    const map = new Map<string, MangaTag>();
    for (const tag of tags) {
      map.set(tag.id, tag);
    }

    return map;
  }

  private createTagGroupsControl() {
    const selectedTags = untracked(() => this.cva.value$()) ?? {
      exclude: [],
      include: [],
    };
    const formRecord = this._fb.record<FormControl<SelectedMangaTag | null>>(
      {}
    );

    const tags = this._tags();
    for (const tag of tags) {
      const control = this._fb.control<SelectedMangaTag | null>(null);

      const isIncluded = selectedTags.include.some((x) => x == tag.id);
      if (isIncluded) {
        control.setValue({ tag, state: 'included' });
      }

      const isExcluded = selectedTags.exclude.some((x) => x == tag.id);
      if (isExcluded) {
        control.setValue({ tag, state: 'excluded' });
      }

      formRecord.addControl(tag.slug, control);
    }

    return formRecord;
  }

  private notifySelectionChanges(
    tags: (SelectedMangaTag | null | undefined)[]
  ) {
    const notNullTags = tags.filter(notNil);

    const included = notNullTags
      .filter((x) => x.state === 'included')
      .map((x) => x.tag.id);
    const excluded = notNullTags
      .filter((x) => x.state === 'excluded')
      .map((x) => x.tag.id);

    this.cva.value = { exclude: excluded, include: included };
  }

  private createTriggerValue(
    selectedTags: SelectedTags | null,
    tagsMap: TagsMap,
    translations: Record<string, string>
  ) {
    if (!selectedTags) {
      return '';
    }

    const { include, exclude } = selectedTags;

    let includedTags = '';
    if (include.length) {
      includedTags =
        `${translations['INCLUDE_TAGS']} ` +
        include.map((x) => tagsMap.get(x)?.name).join(' & ');
    }

    let excludedTags = '';
    if (exclude.length) {
      excludedTags =
        `${translations['EXCLUDE_TAGS']} ` +
        exclude.map((x) => tagsMap.get(x)?.name).join(' & ');
    }

    if (excludedTags && includedTags) {
      const andTranslation = translations['AND'];
      return `${includedTags} ${andTranslation} ${excludedTags}`;
    }

    return `${includedTags}${excludedTags}`;
  }

  private filterTagGroups(searchedTag: string, groups: MangaTagsGroup) {
    if (searchedTag == null) {
      return groups;
    }
    searchedTag = searchedTag.toLowerCase();

    const filteredGroups: MangaTagsGroup = {};

    for (const groupName in groups) {
      const matchedTags = groups[groupName].filter((x) =>
        x.name.toLowerCase().includes(searchedTag)
      );
      if (matchedTags.length !== 0) {
        filteredGroups[groupName] = matchedTags;
      }
    }

    return filteredGroups;
  }
}
