import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { SelectWrapperComponent } from '@shared/ui';
import { MangaTagsFilterComponent } from '../manga-tags-filter/manga-tags-filter.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MangaStatusFilterComponent } from '../manga-status-filter/manga-status-filter.component';
import { MangaSortFilterComponent } from '../manga-sort-filter/manga-sort-filter.component';
import { MangaFilterFieldComponent } from '../manga-filter-field/manga-filter-field.component';
import { AdvancedSearchService } from '../../advanced-search-page/advanced-search.service';
import { TranslocoDirective } from '@jsverse/transloco';

@Component({
  selector: 'moe-search-filters',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatChipsModule,
    MatButtonModule,
    MatIconModule,
    ReactiveFormsModule,
    SelectWrapperComponent,
    MangaTagsFilterComponent,
    MangaStatusFilterComponent,
    MangaSortFilterComponent,
    MangaFilterFieldComponent,
    TranslocoDirective,
  ],
  templateUrl: './search-filters.component.html',
  styleUrl: './search-filters.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchFiltersComponent {
  protected readonly searchService = inject(AdvancedSearchService);
}
