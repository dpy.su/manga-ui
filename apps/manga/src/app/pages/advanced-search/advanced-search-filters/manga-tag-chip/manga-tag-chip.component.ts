import {
  ChangeDetectionStrategy,
  Component,
  computed,
  inject,
  input,
} from '@angular/core';
import { MatChipsModule } from '@angular/material/chips';
import { MangaTag } from '@generated-graphql';
import {
  NgxControlValueAccessor,
  NgxControlValueAccessorCompareTo,
  provideCvaCompareTo,
} from 'ngxtension/control-value-accessor';

export interface SelectedMangaTag {
  tag: MangaTag;
  state: 'included' | 'excluded';
}

const tagComparator: NgxControlValueAccessorCompareTo<SelectedMangaTag> = (
  a,
  b
) => a?.tag.id === b?.tag.id && a?.state === b?.state;

type ChipColorClass =
  | 'manga-tag-chip--included'
  | 'manga-tag-chip--excluded'
  | null;

@Component({
  selector: 'moe-manga-tag-chip',
  standalone: true,
  imports: [MatChipsModule],
  hostDirectives: [NgxControlValueAccessor],
  providers: [provideCvaCompareTo(tagComparator, true)],
  templateUrl: './manga-tag-chip.component.html',
  styleUrl: './manga-tag-chip.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MangaTagChipComponent {
  protected cva = inject<NgxControlValueAccessor<SelectedMangaTag | null>>(
    NgxControlValueAccessor
  );

  protected colorClass = computed<ChipColorClass>(() => {
    const state = this.cva.value$()?.state;

    if (state === 'excluded') {
      return 'manga-tag-chip--excluded';
    } else if (state === 'included') {
      return 'manga-tag-chip--included';
    } else {
      return null;
    }
  });

  protected onChipClick() {
    const value = this.cva.value$();

    if (value == null) {
      this.cva.value = { tag: this.tag(), state: 'included' };
    } else if (value.state === 'included') {
      this.cva.value = { tag: this.tag(), state: 'excluded' };
    } else {
      this.cva.value = null;
    }

    this.cva.markAsTouched();
  }

  public tag = input.required<MangaTag>();
}
