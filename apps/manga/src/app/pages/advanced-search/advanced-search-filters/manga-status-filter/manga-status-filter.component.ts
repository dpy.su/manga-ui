import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MangaStatus } from '@generated-graphql';
import { MatFormField } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { KeyValuePipe } from '@angular/common';
import {
  NgxControlValueAccessor,
  NgxControlValueAccessorCompareTo,
  provideCvaCompareTo,
} from 'ngxtension/control-value-accessor';
import { isEqual } from 'lodash-es';
import { TranslocoDirective } from '@jsverse/transloco';

const statusComparator: NgxControlValueAccessorCompareTo<MangaStatus[]> = (
  a,
  b
) => {
  if (!a && !b) {
    return true;
  }

  if (!a || !b) {
    return false;
  }

  return isEqual(a, b);
};

@Component({
  selector: 'moe-manga-status-filter',
  standalone: true,
  imports: [MatFormField, MatSelectModule, KeyValuePipe, TranslocoDirective],
  hostDirectives: [NgxControlValueAccessor],
  providers: [provideCvaCompareTo(statusComparator, true)],
  templateUrl: './manga-status-filter.component.html',
  styleUrl: './manga-status-filter.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MangaStatusFilterComponent {
  protected readonly mangaStatuses: MangaStatus[] = Object.values(MangaStatus);

  protected cva = inject<NgxControlValueAccessor<MangaStatus[] | null>>(
    NgxControlValueAccessor
  );
}
