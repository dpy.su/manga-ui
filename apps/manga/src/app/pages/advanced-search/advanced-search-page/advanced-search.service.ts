import { computed, inject, Injectable, OnDestroy } from '@angular/core';
import {
  MangaFilter,
  MangaSort,
  MangaSortField,
  MangaStatus,
  SortDirection,
} from '@generated-graphql';
import { FormBuilder } from '@angular/forms';
import { BindQueryParamsFactory } from '@ngneat/bind-query-params';
import { toSignal } from '@angular/core/rxjs-interop';
import { map } from 'rxjs';
import { SortValue } from '../advanced-search-filters/manga-sort-filter/manga-sort-filter.component';
import { SelectedTags } from '../advanced-search-filters/manga-tags-filter/manga-tags-filter.component';

export type AdvancedFilters = MangaFilter & { sort: MangaSort } & {
  page: number;
};

@Injectable()
export class AdvancedSearchService implements OnDestroy {
  private fb = inject(FormBuilder);

  public filters = this.fb.group({
    searchTerm: this.fb.control<string | null>(null),
    statuses: this.fb.control<MangaStatus[] | null>(null),
    sort: this.fb.control<SortValue | null>(null),
    tags: this.fb.control<SelectedTags | null>(null),
    page: this.fb.control(1, { nonNullable: true }),
  });

  public filtersValue = toSignal(
    this.filters.valueChanges.pipe(map(() => this.filters.getRawValue())),
    { initialValue: this.filters.getRawValue() }
  );
  public filtersEmpty = computed(() => {
    const { searchTerm, statuses, sort, tags } = this.filtersValue();
    return (
      !searchTerm &&
      !statuses?.length &&
      !sort &&
      !tags?.include.length &&
      !tags?.exclude.length
    );
  });

  private queryParamsBinder = inject(BindQueryParamsFactory);

  private paramsManager = this.queryParamsBinder
    .create<AdvancedFilters>([
      { queryKey: 'searchTerm', type: 'string' },
      { queryKey: 'statuses', type: 'array' },
      {
        queryKey: 'sort',
        serializer: (val: SortValue | null) => {
          if (!val) {
            return null;
          }

          return `${val.field}.${val.direction}`;
        },
        parser: (val): SortValue | null => {
          const [field, direction] = val.split('.');
          if (
            !field ||
            !direction ||
            !Object.values(MangaSortField).includes(field as MangaSortField) ||
            !Object.values(SortDirection).includes(direction as SortDirection)
          ) {
            return null;
          }

          return {
            field: field as MangaSortField,
            direction: direction as SortDirection,
          };
        },
      },
      {
        queryKey: 'tags',
        serializer: (val: SelectedTags) => {
          if (val.include.length === 0 && val.exclude.length === 0) {
            return null;
          }

          const included = val.include.join(',');
          const excluded = val.exclude.join(',');
          return `${included}&${excluded}`;
        },
        parser: (val): SelectedTags => {
          const [included, excluded] = val.split('&');
          return {
            include: included ? included.split(',') : [],
            exclude: excluded ? excluded.split(',') : [],
          };
        },
      },
      { queryKey: 'page', type: 'number' },
    ])
    .connect(this.filters);

  public ngOnDestroy(): void {
    this.paramsManager.destroy();
  }
}
