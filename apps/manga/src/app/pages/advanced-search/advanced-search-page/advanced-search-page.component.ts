import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  computed,
  DestroyRef,
  inject,
  signal,
  viewChild,
} from '@angular/core';
import { PageHeaderComponent, SearchInputComponent } from '@shared/ui';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { AdvancedSearchService } from './advanced-search.service';
import { ReactiveFormsModule } from '@angular/forms';
import { AdvancedSearchGQL, MangaTileFragment } from '@generated-graphql';
import {
  combineLatest,
  filter,
  map,
  share,
  startWith,
  Subject,
  switchMap,
} from 'rxjs';
import { takeUntilDestroyed, toSignal } from '@angular/core/rxjs-interop';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { MatPaginator } from '@angular/material/paginator';
import { SearchFiltersComponent } from '../advanced-search-filters/search-filters/search-filters.component';
import { MangaGridListComponent } from '@shared/manga';
import { TranslocoPipe } from '@jsverse/transloco';

interface FilterBtnSettings {
  icon: string;
  text: string;
}

type ExpandAnimationState = 'open' | 'closed';

@Component({
  selector: 'moe-advanced-search-page',
  standalone: true,
  imports: [
    MatButtonModule,
    MatIconModule,
    SearchInputComponent,
    SearchFiltersComponent,
    ReactiveFormsModule,
    MangaGridListComponent,
    MatProgressSpinner,
    MatPaginator,
    PageHeaderComponent,
    TranslocoPipe,
  ],
  templateUrl: './advanced-search-page.component.html',
  styleUrl: './advanced-search-page.component.scss',
  animations: [
    trigger('expandFilters', [
      state('closed', style({ height: '0px', minHeight: '0' })),
      state('open', style({ height: '*' })),
      transition('open <=> closed', animate('300ms ease-in-out')),
    ]),
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdvancedSearchPageComponent implements AfterViewInit {
  protected readonly PAGE_SIZE = 10;

  protected searchService = inject(AdvancedSearchService);

  private _destroyRef = inject(DestroyRef);
  private _advancedSearchGQL = inject(AdvancedSearchGQL);
  private _paginator = viewChild.required(MatPaginator);

  private _search = new Subject<void>();
  private _pageChange = new Subject<void>();

  private _queryResult = combineLatest([
    this._search.pipe(startWith(null)),
    this._pageChange.pipe(startWith(null)),
  ]).pipe(
    switchMap(() => {
      const { tags, searchTerm, statuses, sort, page } =
        this.searchService.filtersValue();
      return this._advancedSearchGQL.watch({
        filter: { tags, searchTerm, statuses },
        sort,
        pagination: { page, pageSize: this.PAGE_SIZE },
      }).valueChanges;
    }),
    share()
  );

  private _pageInfo$ = this._queryResult.pipe(
    filter((x) => !x.loading),
    map((x) => {
      const { totalItems, currentPage } = x.data.mangas.pageInfo;
      return { totalItems, currentPage };
    })
  );

  protected showLoader = toSignal<boolean>(
    this._queryResult.pipe(map((x) => x.loading))
  );
  protected manga = toSignal<MangaTileFragment[], []>(
    this._queryResult.pipe(
      filter((x) => !x.loading),
      map((x) => x.data.mangas.items)
    ),
    { initialValue: [] }
  );

  private _showFilters = signal(false);
  protected showFilters = this._showFilters.asReadonly();

  protected animationState = computed<ExpandAnimationState>(() =>
    this._showFilters() ? 'open' : 'closed'
  );

  protected filterBtnSettings = computed<FilterBtnSettings>(() => {
    if (this.showFilters()) {
      return {
        icon: 'expand_less',
        text: 'PAGES.ADVANCED_SEARCH.FILTERS_BTN.HIDE',
      };
    }

    return {
      icon: 'expand_more',
      text: 'PAGES.ADVANCED_SEARCH.FILTERS_BTN.SHOW',
    };
  });

  public ngAfterViewInit() {
    this._paginator()
      .page.pipe(takeUntilDestroyed(this._destroyRef))
      .subscribe((x) => {
        this.searchService.filters.patchValue({ page: x.pageIndex + 1 });
        this._pageChange.next();
      });

    this._pageInfo$
      .pipe(takeUntilDestroyed(this._destroyRef))
      .subscribe(({ totalItems, currentPage }) => {
        this._paginator().length = totalItems;
        this._paginator().pageIndex = currentPage - 1;
      });
  }

  protected toggleShowFilters() {
    this._showFilters.update((x) => !x);
  }

  protected search() {
    this._search.next();
  }

  protected resetFilters() {
    this.searchService.filters.reset();
    this.search();
  }
}
