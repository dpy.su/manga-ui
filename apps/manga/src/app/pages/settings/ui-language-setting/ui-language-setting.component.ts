import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { CommonModule } from '@angular/common';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MatIconModule } from '@angular/material/icon';
import { LanguageService } from '@core/localization';
import { LanguageIconComponent } from '@shared/ui';
import { TranslocoDirective } from '@jsverse/transloco';

@Component({
  selector: 'moe-ui-language-setting',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatIconModule,
    LanguageIconComponent,
    TranslocoDirective,
  ],
  templateUrl: './ui-language-setting.component.html',
  styleUrls: ['./ui-language-setting.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UiLanguageSettingComponent {
  protected readonly languageService = inject(LanguageService);

  protected readonly languageControl = new FormControl<string>(
    this.languageService.currentLanguage(),
    { nonNullable: true }
  );

  constructor() {
    this.languageControl.valueChanges
      .pipe(takeUntilDestroyed())
      .subscribe((lang) => this.languageService.setCurrentLanguage(lang));
  }
}
