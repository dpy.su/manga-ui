import { Routes } from '@angular/router';
import { SettingsPageComponent } from './settings-page/settings-page.component';

export default [
  {
    path: '',
    component: SettingsPageComponent,
    title: 'PAGES.SETTINGS.TITLE',
  },
] satisfies Routes;
