import { ChangeDetectionStrategy, Component, input } from '@angular/core';

@Component({
  selector: 'moe-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss'],
  standalone: true,

  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SettingComponent {
  public title = input.required();
  public description = input.required();
}
