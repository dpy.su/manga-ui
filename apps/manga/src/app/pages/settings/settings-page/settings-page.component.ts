import {
  ChangeDetectionStrategy,
  Component,
  computed,
  inject,
  Type,
} from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { SettingComponent } from '../setting/setting.component';
import { CommonModule } from '@angular/common';
import { PageHeaderComponent, SearchInputComponent } from '@shared/ui';
import { UiLanguageSettingComponent } from '../ui-language-setting/ui-language-setting.component';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { NonNullableFormBuilder, ReactiveFormsModule } from '@angular/forms';
import { toSignal } from '@angular/core/rxjs-interop';
import { ThemeSettingComponent } from '../theme-setting/theme-setting.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';

interface SettingGroup {
  titleKey: string;
  icon: string;
  settings: Setting[];
}

interface Setting {
  titleKey: string;
  descriptionKey: string;
  /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
  component: Type<any>;
}

@Component({
  selector: 'moe-settings-page',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    SettingComponent,
    SearchInputComponent,
    PageHeaderComponent,
    TranslocoDirective,
  ],
  templateUrl: './settings-page.component.html',
  styleUrl: './settings-page.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SettingsPageComponent {
  private _fb = inject(NonNullableFormBuilder);
  private _translateService = inject(TranslocoService);

  protected ALL_SETTINGS_GROUP_KEY = 'PAGES.SETTINGS.SETTING_GROUPS.ALL';

  protected readonly settingGroups: readonly SettingGroup[] = [
    {
      titleKey: 'PAGES.SETTINGS.SETTING_GROUPS.LANGUAGE',
      icon: 'language',
      settings: [
        {
          titleKey: 'PAGES.SETTINGS.UI_LANGUAGE_SETTING.TITLE',
          descriptionKey: 'PAGES.SETTINGS.UI_LANGUAGE_SETTING.DESCRIPTION',
          component: UiLanguageSettingComponent,
        },
      ],
    },
    {
      titleKey: 'PAGES.SETTINGS.SETTING_GROUPS.DISPLAY',
      icon: 'invert_colors',
      settings: [
        {
          titleKey: 'PAGES.SETTINGS.THEME_SETTING.TITLE',
          descriptionKey: 'PAGES.SETTINGS.THEME_SETTING.DESCRIPTION',
          component: ThemeSettingComponent,
        },
      ],
    },
  ];

  protected searchbarControl = this._fb.control('');
  protected selectedGroupControl = this._fb.control([
    this.ALL_SETTINGS_GROUP_KEY,
  ]);

  protected searchTerm = toSignal(this.searchbarControl.valueChanges);
  protected selectedGroupKey = toSignal(
    this.selectedGroupControl.valueChanges,
    { initialValue: this.selectedGroupControl.value }
  );
  protected selectedGroupSettings = computed(() => {
    const key = this.selectedGroupKey()[0];
    const term = this.searchTerm()?.toLowerCase();

    if (key == this.ALL_SETTINGS_GROUP_KEY) {
      const settings = this.settingGroups.flatMap((x) => x.settings);
      return this.filterSettingsByName(settings, term);
    }

    const settings = this.settingGroups
      .filter((x) => x.titleKey == key)
      .flatMap((x) => x.settings);

    return this.filterSettingsByName(settings, term);
  });

  private filterSettingsByName(settings: Setting[], name?: string | null) {
    if (name == null) {
      return settings;
    }

    return settings.filter(
      (x) =>
        this._translateService
          .translate(x.titleKey)
          .toLowerCase()
          .includes(name) ||
        this._translateService
          .translate(x.descriptionKey)
          .toLowerCase()
          .includes(name)
    );
  }
}
