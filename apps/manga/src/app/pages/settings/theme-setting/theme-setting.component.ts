import {
  ChangeDetectionStrategy,
  Component,
  effect,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { Theme, ThemeService } from '@core/theming';
import { TranslocoDirective } from '@jsverse/transloco';

@Component({
  selector: 'moe-theme-setting',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatIconModule,
    TranslocoDirective,
  ],
  templateUrl: './theme-setting.component.html',
  styleUrl: './theme-setting.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ThemeSettingComponent {
  protected readonly themeService = inject(ThemeService);

  protected readonly themeControl = new FormControl<Theme>(
    this.themeService.theme(),
    { nonNullable: true }
  );

  constructor() {
    effect(() => {
      const theme = this.themeService.theme();
      this.themeControl.setValue(theme, { emitEvent: false });
    });

    this.themeControl.valueChanges
      .pipe(takeUntilDestroyed())
      .subscribe((theme) =>
        theme === 'dark'
          ? this.themeService.setDark()
          : this.themeService.setLight()
      );
  }
}
