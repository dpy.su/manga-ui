import { Routes } from '@angular/router';
import { BookmarksPageComponent } from './bookmarks-page/bookmarks-page.component';

export default [
  {
    path: '',
    component: BookmarksPageComponent,
    title: 'PAGES.BOOKMARKS.TITLE',
  },
] satisfies Routes;
