import {
  ChangeDetectionStrategy,
  Component,
  inject,
  input,
  linkedSignal,
} from '@angular/core';

import { combineLatest, filter, map, share, switchMap } from 'rxjs';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import { MangaGridListComponent } from '@shared/manga';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import {
  BookmarksGQL,
  MangaBookmarkStatus,
  MangaTileFragment,
  PagePaginationInfo,
} from '@generated-graphql';
import { Pagination } from '@core/helpers';

@Component({
  selector: 'moe-bookmarks-list',
  standalone: true,
  imports: [MangaGridListComponent, MatPaginator, MatProgressSpinner],
  templateUrl: './bookmarks-list.component.html',
  styleUrl: './bookmarks-list.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BookmarksListComponent {
  protected readonly PAGE_SIZE = 10;

  public bookmarkStatus = input.required<MangaBookmarkStatus>();

  private _bookmarksGQL = inject(BookmarksGQL);

  private pagination = linkedSignal<MangaBookmarkStatus, Pagination>({
    source: () => this.bookmarkStatus(),
    computation: () => ({ pageSize: this.PAGE_SIZE, page: 1, }),
  });

  private _queryResult = combineLatest([
    toObservable(this.bookmarkStatus),
    toObservable(this.pagination),
  ]).pipe(
    switchMap(([status, pagination]) => {
      return this._bookmarksGQL.watch({
        status,
        pagination: { pageSize: pagination.pageSize, page: pagination.page },
      }).valueChanges;
    }),
    share()
  );

  protected showLoader = toSignal(this._queryResult.pipe(map(x => x.loading)));
  protected manga = toSignal<MangaTileFragment[] | null>(
    this._queryResult.pipe(
      filter(x => !x.loading || x.data != null),
      map(x => x.data.myBookmarks.items.map(i => i.manga))
    ),
    { initialValue: null }
  );
  protected pageInfo = toSignal<PagePaginationInfo | null>(
    this._queryResult.pipe(
      filter(x => !x.loading || x.data != null),
      map(x => x.data.myBookmarks.pageInfo)
    ),
    { initialValue: null }
  );

  protected onPageChange(event: PageEvent) {
    this.pagination.set({
      pageSize: event.pageSize,
      page: event.pageIndex + 1,
    });
  }
}
