import { ChangeDetectionStrategy, Component } from '@angular/core';
import { PageHeaderComponent } from '@shared/ui';
import { MatTab, MatTabContent, MatTabGroup } from '@angular/material/tabs';
import { MangaBookmarkStatus } from '@generated-graphql';
import { BookmarksListComponent } from '../bookmarks-list/bookmarks-list.component';
import { TranslocoDirective } from '@jsverse/transloco';

const BOOKMARK_STATUSES = Object.values<MangaBookmarkStatus>(
  MangaBookmarkStatus
)
  .sort()
  .reverse();

@Component({
  selector: 'moe-bookmarks-page',
  standalone: true,
  imports: [
    PageHeaderComponent,
    MatTabGroup,
    MatTabContent,
    MatTab,
    BookmarksListComponent,
    TranslocoDirective,
  ],
  templateUrl: './bookmarks-page.component.html',
  styleUrl: './bookmarks-page.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BookmarksPageComponent {
  protected readonly bookmarkStatuses = BOOKMARK_STATUSES;
}
