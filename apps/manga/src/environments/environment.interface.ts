export interface Environment {
  baseUrl: string;
  isProduction: boolean;
  graphqlUrl: string;
  stsConfig: {
    authority: string;
    secureRoutes: string[];
  };
}
