import { Environment } from './environment.interface';

export const environment: Environment = {
  baseUrl: 'http://localhost:4200',
  isProduction: false,
  graphqlUrl: 'https://graphql-manga.dpy.su/',
  stsConfig: {
    authority: 'https://auth.dpy.su/realms/manga-staging',
    secureRoutes: ['https://graphql-manga.dpy.su'],
  },
};
