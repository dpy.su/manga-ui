import { Environment } from './environment.interface';

export const environment: Environment = {
  baseUrl: 'https://manga.dpy.su',
  isProduction: true,
  graphqlUrl: 'https://graphql-manga.dpy.su/',
  stsConfig: {
    authority: 'https://auth.dpy.su/realms/manga-staging',
    secureRoutes: ['https://graphql-manga.dpy.su'],
  },
};
